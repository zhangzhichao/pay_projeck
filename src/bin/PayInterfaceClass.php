<?php

namespace AgilePayments\bin;

use AgilePayments\bin\format\PayReturn;
use AgilePayments\bin\format\RefundQueryFormat;
use AgilePayments\bin\format\ReturnFormat;

interface PayInterfaceClass
{
    function pay(...$arge): PayReturn;

    function refund(array $payInfo, int $price, string $notify): RefundQueryFormat;

    function close(string $outOrderId, string $time): ReturnFormat;

    function query(string $outOrderId, string $time): ReturnFormat;

    function refundQuery(string $refundNo, string $outOrderId): RefundQueryFormat;
}