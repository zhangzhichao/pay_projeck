<?php

namespace AgilePayments\bin;

use AgilePayments\Tool;

class AgilePaymentLog
{
    public static function writeLog($data = '', $type = 'info'): void
    {
        if (Tool::env('AgilePayment.writePaymentLog', 'true') == 'true') {
            Tool::log($data, $type, 'payment/log');
        }
    }
    
}