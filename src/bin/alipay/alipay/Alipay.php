<?php

namespace AgilePayments\bin\alipay\alipay;

use AgilePayments\bin\AgilePaymentException;
use AgilePayments\bin\AgilePaymentLog;
use AgilePayments\bin\alipay\alipay\AlipayBase;
use AgilePayments\bin\extend\alipay\request\AlipayFundAuthOperationDetailQueryRequest;
use AgilePayments\bin\extend\alipay\request\AlipayFundAuthOrderUnfreezeRequest;
use AgilePayments\bin\extend\alipay\request\AlipayFundTransUniTransferRequest;
use AgilePayments\bin\extend\alipay\request\AlipayTradeCloseRequest;
use AgilePayments\bin\extend\alipay\request\AlipayTradeFastpayRefundQueryRequest;
use AgilePayments\bin\extend\alipay\request\AlipayTradePayRequest;
use AgilePayments\bin\extend\alipay\request\AlipayTradeQueryRequest;
use AgilePayments\bin\extend\alipay\request\AlipayTradeRefundRequest;
use AgilePayments\bin\format\RefundQueryFormat;
use AgilePayments\bin\PayInterfaceClass;
use AgilePayments\bin\format\PayReturn;
use AgilePayments\bin\format\ReturnFormat;
use AgilePayments\Config;
use AgilePayments\Tool;

class Alipay extends AlipayBase implements PayInterfaceClass
{

    public function __construct(Config $config)
    {
        parent::__construct($config);
    }

    /**
     * 支付路由
     * @return PayReturn
     */
    public function pay(...$arge): PayReturn
    {
        $platform = strtolower($this->config->platform);
        $payment = strtolower($this->config->payment);
        $payType = strtolower($this->config->payType);
        $class = "\\AgilePayments\\bin\\{$platform}\\{$payment}\\Pay";
        if (!class_exists($class)) {
            throw new AgilePaymentException("调用类[$class]不存在！");
        }
        if (!method_exists($class, $payType)) {
            throw new AgilePaymentException("调用方法[$class::$payType()]不存在！");
        }
        AgilePaymentLog::writeLog($arge, "{$this->config->platform} request: alipay.trade.appPayRequest");
        $data = call_user_func_array([new $class($this->config), $payType], $arge);
        AgilePaymentLog::writeLog($data, "{$this->config->platform} response");
        return $data;
    }

    /**
     * 退款
     * @param array $payInfo
     * @param int $price
     * @param string $notifyUrl
     * @return RefundQueryFormat
     * @throws AgilePaymentException
     */
    public function refund(array $payInfo, int $price, string $notifyUrl = ''): RefundQueryFormat
    {
        try {
            $object = new \stdClass();
            $object->out_trade_no = $payInfo['out_order_id'];
            $object->out_request_no = $payInfo['refund_order_no'];
            $object->refund_amount = $this->fenToYuan($payInfo['pay_amount']);
            $object->refund_reason = $payInfo['refund_reason'];
            $json = json_encode($object);
            $request = new AlipayTradeRefundRequest();
            $request->setBizContent($json);
            $result = $this->aop->execute($request);
            $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
            $resultCode = $result->$responseNode->code;
            if (!empty($resultCode) && $resultCode == 10000) {
                $status = false;
                if ($result->$responseNode->fund_change == 'Y') {
                    $status = true;
                }
                $msg = $result->$responseNode->msg ?? '';
                $data = [
                    'refundStatus' => $result->$responseNode->fund_change == 'Y' ? 'REFUND_SUCCESS' : $result->$responseNode->fund_change,
                    'outOrderId' => $result->$responseNode->out_trade_no,
                    'tradeNo' => $result->$responseNode->trade_no,
                    'refundAmount' => $this->yuanToFen($result->$responseNode->send_back_fee ?? '0'),
                    'totalAmount' => $this->yuanToFen($result->$responseNode->refund_fee ?? '0'),
                ];
            } else {
                $msg = $result->$responseNode->sub_msg ?? '';
                throw new AgilePaymentException($msg, 0, func_get_args());
            }
        } catch (Exception $e) {
            throw new AgilePaymentException($e->getMessage(), 0, func_get_args());
        } catch (\Error $e) {
            throw new AgilePaymentException($e->getMessage(), 0, func_get_args());
        }
        return new RefundQueryFormat ($data, $status, $msg);
    }

    /**
     * 关闭订单
     * @param string $outOrderId 原支付订单号
     * @param string $time 兼容参数
     * @return ReturnFormat
     * @throws AgilePaymentException
     */
    public function close(string $outOrderId, string $time = ''): ReturnFormat
    {
        try {
            $object = new \stdClass();
            $object->out_trade_no = $outOrderId;
            $json = json_encode($object);
            $request = new AlipayTradeCloseRequest();
            $request->setBizContent($json);
            $result = $this->aop->execute($request);
            $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
            $resultCode = $result->$responseNode->code;
            if (!empty($resultCode) && $resultCode == 10000) {
                $status = true;
                $msg = $result->$responseNode->msg ?? '';
                $data = [
                    "outOrderId" => $result->$responseNode->out_trade_no,
                    "tradeNo" => $result->$responseNode->trade_no,
                ];
            } else {
                $msg = $result->$responseNode->sub_msg ?? '';
                throw new AgilePaymentException($msg, 0, func_get_args());
            }
        } catch (Exception $e) {
            throw new AgilePaymentException($e->getMessage(), 0, func_get_args());
        } catch (\Error $e) {
            throw new AgilePaymentException($e->getMessage(), 0, func_get_args());
        }
        return new ReturnFormat($data, $status, $msg);
    }


    /**
     * 订单查询
     * @param string $outOrderId 支付订单号
     * @param string $time 兼容参数
     * @return ReturnFormat
     * @throws AgilePaymentException
     */
    public function query(string $outOrderId, string $time = ''): ReturnFormat
    {
        try {
            $object = new \stdClass();
            $object->out_trade_no = $outOrderId;
            $json = json_encode($object);
            $request = new AlipayTradeQueryRequest();
            $request->setBizContent($json);
            $result = $this->aop->execute($request);
            $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
            $resultCode = $result->$responseNode->code;
            if (!empty($resultCode) && $resultCode == 10000) {
                $status = true;
                $msg = $result->$responseNode->msg ?? '';
                $data = $result->$responseNode;
                $data = [
                    "date" => $result->$responseNode->send_pay_date,
                    "amount" => $this->yuanToFen($result->$responseNode->total_amount),
                    "tradeNo" => $result->$responseNode->trade_no,
                    "outOrderId" => $result->$responseNode->out_trade_no,
                    "tradeStatus" => $result->$responseNode->trade_status
                ];
            } else {
                $msg = $result->$responseNode->sub_msg ?? '';
                throw new AgilePaymentException($msg, 0, func_get_args());
            }

        } catch (Exception $e) {
            throw new AgilePaymentException($e->getMessage(), 0, func_get_args());
        } catch (\Error $e) {
            throw new AgilePaymentException($e->getMessage(), 0, func_get_args());
        }
        return new ReturnFormat($data, $status, $msg);
    }

    /**
     * @param string $refundNo 退款订单号
     * @param string $outOrderId 支付订单号
     * @return RefundQueryFormat
     * @throws AgilePaymentException
     */
    public function refundQuery(string $refundNo, string $outOrderId): RefundQueryFormat
    {
        try {

            $object = new \stdClass();
            $object->out_trade_no = $outOrderId;
            $object->out_request_no = $refundNo;
            $json = json_encode($object);
            $request = new AlipayTradeFastpayRefundQueryRequest();
            $request->setBizContent($json);
            $result = $this->aop->execute($request);
            $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
            $resultCode = $result->$responseNode->code;
            if (!empty($resultCode) && $resultCode == 10000) {

                $status = false;
                if (!empty($result->$responseNode->refund_status) && $result->$responseNode->refund_status == 'REFUND_SUCCESS') {
                    $status = true;
                }
                $msg = $result->$responseNode->msg ?? '';
                $data = [
                    'refundStatus' => $result->$responseNode->refund_status ?? '-1',
                    'outOrderId' => $result->$responseNode->out_trade_no ?? '-1',
                    'refundNo' => $result->$responseNode->out_request_no ?? '-1',
                    'tradeNo' => $result->$responseNode->trade_no ?? '-1',
                    'refundAmount' => $this->yuanToFen($result->$responseNode->refund_amount ?? 0),
                    'totalAmount' => $this->yuanToFen($result->$responseNode->total_amount ?? 0),
                ];
            } else {
                $msg = $result->$responseNode->subMsg ?? '';
                throw new AgilePaymentException($msg, 0, func_get_args());
            }
        } catch (Exception $e) {
            throw new AgilePaymentException($e->getMessage(), 0, func_get_args());
        } catch (\Error $e) {
            throw new AgilePaymentException($e->getMessage(), 0, func_get_args());
        }
        return new RefundQueryFormat($data, $status, $msg);
    }


    public function freezeQuery(string $outOrderId, string $type = 'FREEZE')
    {
        try {
            $object = new \stdClass();
            $object->out_order_no = $outOrderId;
            $object->out_request_no = $outOrderId;
            $object->operation_type = $type;
            $json = json_encode($object);
            $request = new AlipayFundAuthOperationDetailQueryRequest();
            $request->setBizContent($json);
            $result = $this->aop->execute($request);
            $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
            $resultCode = $result->$responseNode->code;

            if (!empty($resultCode) && $resultCode == 10000) {
                $status = true;
                $msg = $result->$responseNode->msg ?? '';
                $data = [
                    "amount" => $result->$responseNode->amount??'',
                    "auth_no" => $result->$responseNode->auth_no??'',
                    "extra_param" => $result->$responseNode->extra_param??'',
                    "gmt_create" => $result->$responseNode->gmt_create??'',
                    "gmt_trans" => $result->$responseNode->gmt_trans??'',
                    "operation_id" => $result->$responseNode->operation_id??'',
                    "operation_type" => $result->$responseNode->operation_type??'',
                    "order_status" => $result->$responseNode->order_status??'',
                    "order_title" => $result->$responseNode->order_title??'',
                    "out_order_no" => $result->$responseNode->out_order_no??'',
                    "out_request_no" => $result->$responseNode->out_request_no??'',
                    "payer_logon_id" => $result->$responseNode->payer_logon_id??'',
                    "payer_user_id" => $result->$responseNode->payer_user_id??'',
                    "remark" => $result->$responseNode->remark??'',
                    "rest_amount" => $result->$responseNode->rest_amount??'',
                    "status" => $result->$responseNode->status??'',
                    "total_freeze_amount" => $result->$responseNode->total_freeze_amount??'',
                    "total_pay_amount" => $result->$responseNode->total_pay_amount??'',
                ];
            } else {
                $msg = $result->$responseNode->sub_msg ?? '';
                throw new AgilePaymentException($msg, 0, func_get_args());
            }

        } catch (Exception $e) {
            throw new AgilePaymentException($e->getMessage(), 0, func_get_args());
        } catch (\Error $e) {
            throw new AgilePaymentException($e->getMessage(), 0, func_get_args());
        }
        return new ReturnFormat($data, $status, $msg);


    }

    /**
     * @desc 资金授权解冻接口
     * @param string $authNo
     * @param string $outUnfreezeOrderId
     * @param string $amount (分)
     * @param string $remark
     * @return ReturnFormat
     * @throws AgilePaymentException
     */
    public function unfreeze(string $authNo, string $outUnfreezeOrderId, string $amount, string $remark)
    {
        try {
            $object = new \stdClass();
            $object->auth_no = $authNo;
            $object->out_request_no = $outUnfreezeOrderId;
            $object->amount = $this->fenToYuan($amount);
            $object->remark = $remark;
            $json = json_encode($object);
            $request = new AlipayFundAuthOrderUnfreezeRequest();
            $request->setBizContent($json);
            $result = $this->aop->execute($request);
            $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
            $resultCode = $result->$responseNode->code;

            if (!empty($resultCode) && $resultCode == 10000) {
                $status = true;
                $msg = $result->$responseNode->msg ?? '';
                $data = [
                    "amount" => $result->$responseNode->amount??'',
                    "auth_no" => $result->$responseNode->auth_no??'',
                    "extra_param" => $result->$responseNode->extra_param??'',
                    "gmt_create" => $result->$responseNode->gmt_create??'',
                    "gmt_trans" => $result->$responseNode->gmt_trans??'',
                    "operation_id" => $result->$responseNode->operation_id??'',
                    "operation_type" => $result->$responseNode->operation_type??'',
                    "order_status" => $result->$responseNode->order_status??'',
                    "order_title" => $result->$responseNode->order_title??'',
                    "out_order_no" => $result->$responseNode->out_order_no??'',
                    "out_request_no" => $result->$responseNode->out_request_no??'',
                    "payer_logon_id" => $result->$responseNode->payer_logon_id??'',
                    "payer_user_id" => $result->$responseNode->payer_user_id??'',
                    "remark" => $result->$responseNode->remark??'',
                    "rest_amount" => $result->$responseNode->rest_amount??'',
                    "status" => $result->$responseNode->status??'',
                    "total_freeze_amount" => $result->$responseNode->total_freeze_amount??'',
                    "total_pay_amount" => $result->$responseNode->total_pay_amount??'',
                ];
            } else {
                $msg = $result->$responseNode->sub_msg ?? '';
                throw new AgilePaymentException($msg, 0, func_get_args());
            }

        } catch (Exception $e) {
            throw new AgilePaymentException($e->getMessage(), 0, func_get_args());
        } catch (\Error $e) {
            throw new AgilePaymentException($e->getMessage(), 0, func_get_args());
        }
        return new ReturnFormat($data, $status, $msg);

    }


    /**
     * @param string $authNo
     * @param string $outFreezePayOrderId
     * @param string $amount （分）
     * @param string $remark
     * @param string $type
     * @return void
     * @throws AgilePaymentException
     */
    public function freezePay(string $authNo, string $outFreezePayOrderId, string $amount, string $remark, string $notifyUrl = '', string $type = 'NOT_COMPLETE')
    {
        try {
            $object = new \stdClass();
            $object->auth_no = $authNo;
            $object->out_trade_no = $outFreezePayOrderId;
            $object->total_amount = $this->fenToYuan($amount);
            $object->subject = $remark;
            $object->product_code = 'PREAUTH_PAY';
            $object->auth_confirm_mode = $type;
            $json = json_encode($object);
            $request = new AlipayTradePayRequest();
            $request->setNotifyUrl($notifyUrl);
            $request->setBizContent($json);
            $result = $this->aop->execute($request);
            $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
            $resultCode = $result->$responseNode->code;

            if (!empty($resultCode) && $resultCode == 10000) {
                $status = true;
                $msg = $result->$responseNode->msg ?? '';
                $data = [
                    "buyer_logon_id" => $result->$responseNode->buyer_logon_id??'',
                    "buyer_pay_amount" => $result->$responseNode->buyer_pay_amount??'',
                    "buyer_user_id" => $result->$responseNode->buyer_user_id??'',
                    "buyer_user_type" => $result->$responseNode->buyer_user_type??'',
                    "fund_bill_list" => $result->$responseNode->fund_bill_list??'',
                    "gmt_payment" => $result->$responseNode->gmt_payment??'',
                    "invoice_amount" => $result->$responseNode->invoice_amount??'',
                    "out_trade_no" => $result->$responseNode->out_trade_no??'',
                    "point_amount" => $result->$responseNode->point_amount??'',
                    "receipt_amount" => $result->$responseNode->receipt_amount??'',
                    "total_amount" => $result->$responseNode->total_amount??'',
                    "trade_no" => $result->$responseNode->trade_no??'',
                ];
            } else {
                $msg = $result->$responseNode->sub_msg ?? '';
                throw new AgilePaymentException($msg, 0, func_get_args());
            }

        } catch (Exception $e) {
            throw new AgilePaymentException($e->getMessage(), 0, func_get_args());
        } catch (\Error $e) {
            throw new AgilePaymentException($e->getMessage(), 0, func_get_args());
        }
        return new ReturnFormat($data, $status, $msg);

    }

    /**
     * @param string $outOrderId
     * @param int $amount (分)
     * @param string $title
     * @param array $payeeInfo ['user_id'=> 9527] or ['open_id' => 9527]
     * @param string $name 接收方名称[姓名|企业名称]
     * @return ReturnFormat
     * @throws AgilePaymentException
     */
    public function AlipayFundTransUniTransferRequest(string $outOrderId, string $amount, string $title, array $payeeInfo, string $name = '')
    {
        if ($amount < 10){
            throw new AgilePaymentException('转账金额不能低于0.1元');
        }

        if (count($payeeInfo) != 1 || !in_array(array_key_first($payeeInfo),['user_id','open_id','login_id'])) {
            throw new AgilePaymentException('payeeInfo 参数错误!');
        }
        if (array_key_first($payeeInfo) == 'user_id'){
            $info['identity'] = $payeeInfo['user_id'];
            $info['identity_type'] = 'ALIPAY_USER_ID';
        }elseif(array_key_first($payeeInfo) == 'login_id'){
            if ($name == ''){
                throw new AgilePaymentException('参与方真实姓名不能为空!');
            }
            $info['identity'] = $payeeInfo['login_id'];
            $info['name'] = $name;
            $info['identity_type'] = 'ALIPAY_LOGON_ID';
        }else{
            $info['identity'] = $payeeInfo['open_id'];
            $info['identity_type'] = 'ALIPAY_OPEN_ID';
        }

        try {
            $object = new \stdClass();
            $object->out_biz_no = $outOrderId;
            $object->trans_amount = $this->fenToYuan($amount);
            $object->biz_scene = 'DIRECT_TRANSFER';
            $object->product_code = 'TRANS_ACCOUNT_NO_PWD';
            $object->order_title = $title;
            $object->remark = $title;
            $object->payee_info = $info;
            $json = json_encode($object);
            $request = new AlipayFundTransUniTransferRequest();
            $request->setBizContent($json);
            $result = $this->aop->execute($request);
            $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
            $resultCode = $result->$responseNode->code;

            if (!empty($resultCode) && $resultCode == 10000) {
                $status = true;
                $msg = $result->$responseNode->msg ?? '';
                if ($result->$responseNode->status != 'SUCCESS'){
                    $status = false;
                    $msg = $result->$responseNode->sub_msg ?? '';
                }
                $data = [
                    "outOrderId" => $result->$responseNode->out_biz_no??'',
                    "orderId" => $result->$responseNode->order_id??'',
                    "pay_fund_order_id" => $result->$responseNode->pay_fund_order_id??'',
                    "status" => $result->$responseNode->status??'',
                    "trans_date" => $result->$responseNode->trans_date??'',
                ];
            } else {
                $msg = $result->$responseNode->sub_msg ?? '';
                throw new AgilePaymentException($msg, 0, func_get_args());
            }

        } catch (Exception $e) {
            throw new AgilePaymentException($e->getMessage(), 0, func_get_args());
        } catch (\Error $e) {
            throw new AgilePaymentException($e->getMessage(), 0, func_get_args());
        }
        return new ReturnFormat($data, $status, $msg);
    }


    public function verifySign()
    {
        $data = $_POST;
        unset($data['payType']);
        $status = $this->aop->rsaCheckV1($data,$this->config->payPublicCertPath,$this->aop->signType);
        if ($status){
            $status = true;
        }else{
            $status = false;
        }
        return [
            'status'        => $status,
            'payOrderId'    => $_POST['out_trade_no']??$_POST['out_order_no']??$_POST['out_request_no']??'', // order_id
            'trade_no'      => $_POST['trade_no']??$_POST['operation_id']??'',
            'query_id'      => $_POST['queryId']??'',
            'data'          => $_POST??[],
        ];
    }

    public function returnSuccess()
    {
        echo 'success';
    }


    public function __call($name, $arguments)
    {
        throw new AgilePaymentException("调用方法[" . self::class . ":{$name}()]不存在！");
    }
}
