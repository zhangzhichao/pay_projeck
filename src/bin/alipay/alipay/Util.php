<?php

namespace AgilePayments\bin\alipay\alipay;

use AgilePayments\bin\AgilePaymentException;
use AgilePayments\bin\extend\alipay\request\AlipaySystemOauthTokenRequest;
use AgilePayments\bin\extend\alipay\request\AlipayTradeOrderSettleRequest;
use AgilePayments\bin\extend\alipay\request\AlipayTradeRoyaltyRelationBatchqueryRequest;
use AgilePayments\bin\extend\alipay\request\AlipayTradeRoyaltyRelationUnbindRequest;
use AgilePayments\bin\extend\alipay\request\AlipayTradeSettleConfirmRequest;
use AgilePayments\bin\extend\alipay\request\AlipayUserInfoAuthRequest;
use AgilePayments\bin\extend\alipay\request\AlipayUserInfoShareRequest;
use AgilePayments\bin\extend\alipay\request\AntMerchantExpandIndirectZftCreateRequest;
use AgilePayments\bin\extend\alipay\request\AntMerchantExpandIndirectZftModifyRequest;
use AgilePayments\bin\extend\alipay\request\AntMerchantExpandIndirectZftorderQueryRequest;
use AgilePayments\bin\extend\alipay\request\AntMerchantExpandIndirectImageUploadRequest;
use AgilePayments\bin\extend\alipay\request\AntMerchantExpandIndirectZftConsultRequest;
use AgilePayments\bin\extend\alipay\request\AlipayTradeRoyaltyRelationBindRequest;
use AgilePayments\bin\extend\alipay\request\AntMerchantExpandIndirectZftDeleteRequest;
use AgilePayments\bin\extend\alipay\request\AntMerchantExpandIndirectZftSettlementmodifyRequest;
use AgilePayments\Config;
use AgilePayments\Tool;
use Alipay\EasySDK\Kernel\Util\ResponseChecker;

class Util extends AlipayBase
{
    public function __construct(Config $config)
    {
        parent::__construct($config);
    }

    /**
     * @desc 获取ali用户编号
     * @param string $code 授权码
     * @param bool $getOpenId 是否获取openId 兼容支付宝逐渐弃用userId
     * @return mixed|string
     * @throws \Exception
     */
    public function getAlipayUserId(string $code, bool $getOpenId = false)
    {
        $request = new AlipaySystemOauthTokenRequest();
        $request->setCode($code);
        $request->setGrantType("authorization_code");

        $responseResult = $this->aop->execute($request);

        if (!isset($responseResult->error_response)) {
            $responseApiName = str_replace(".", "_", $request->getApiMethodName()) . "_response";
            $response = $responseResult->$responseApiName;

            $data = [
                'access_token' => $response->access_token ?? '',
                'refresh_token' => $response->refresh_token ?? '',
                'user_id' => $response->user_id ?? '',
                'open_id' => $response->open_id ?? '',
            ];
            return [
                'status' => true,
                'msg' => 'success',
                'data' => $data ?? [],
            ];
        } else {
            throw new AgilePaymentException($responseResult->error_response->sub_msg, 0, func_get_args());
        }
    }

    public function getUserInfo(string $token)
    {
        try {
            $request = new AlipayUserInfoShareRequest();

            $result = $this->aop->execute($request, $token);

            $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
            $resultCode = $result->$responseNode->code;
            if (!empty($resultCode) && $resultCode == 10000) {
                return $result->$responseNode;
            } else {
                throw new AgilePaymentException($result->$responseNode->sub_msg ?? $result->$responseNode->msg ?? '', 0, func_get_args());
            }
        } catch (\Exception $e) {
            throw new AgilePaymentException($e->getMessage(), 0, func_get_args());
        } catch (\Error $e) {
            throw new AgilePaymentException($e->getMessage(), 0, func_get_args());
        }

    }

    public function getLoginPage()
    {
        $obj = new \stdClass();
        $obj->scopes = ['auth_base'];
        $request = new AlipayUserInfoAuthRequest();
        $request->setBizContent(json_encode($obj));
        $result = $this->aop->pageExecute($request);
        echo $result;
        die;
    }

    public function verifySign()
    {

//        $str = 'gmt_create=2024-05-15+17%3A08%3A18&charset=utf-8&seller_email=wyx%40headcollect.com&subject=CGP00308767&sign=ubJh052v5EUcX2%2Bfe3Q5sSVL6kECPwCg60GkIZ7ygwN5%2FeR0XY6qJakXQNrI79sBkCoDCrcduKE7eP2BO4UXrmNW3bQvot2hDgjRnXOxBfRCtJmFbPiWBJVCcmn5O4ZdutdcI36WZmd7WZZ9Ywf%2FhorgOXze8YysrSBcG3CojCqW0VWsHVUlAIN18m1yJlrS0Ss7hvKq9aRtzN6N%2FB9kJ3kLr6ICLCo2292mEtMN%2BHboenYLmb60BG8mtqCakKVxETJvyta%2BQLJdO1vbQ1yji91iEQ5Ke%2F39yNZkKcxxr3S0xvD6lLXwUsNmMiKKO4sAAjNDNnpezxEKizhEPVe3Vw%3D%3D&buyer_id=2088802626703271&invoice_amount=0.01&notify_id=2024051501222170819003271406046334&fund_bill_list=%5B%7B%22amount%22%3A%220.01%22%2C%22fundChannel%22%3A%22ALIPAYACCOUNT%22%7D%5D&notify_type=trade_status_sync&trade_status=TRADE_SUCCESS&receipt_amount=0.01&app_id=2021004135640033&buyer_pay_amount=0.01&sign_type=RSA2&seller_id=2088741768513033&gmt_payment=2024-05-15+17%3A08%3A19&notify_time=2024-05-15+17%3A08%3A19&passback_params=%257B%2522payType%2522%253A%2522Alipay.Alipay.App%2522%252C%2522payOrderId%2522%253A%2522TESTO5SDIS1O39754297%2522%252C%2522configId%2522%253A19%257D&version=1.0&out_trade_no=TESTO5SDIS1O39754297&total_amount=0.01&trade_no=2024051522001403271414062991&auth_app_id=2021004135640033&buyer_logon_id=188****2238&point_amount=0.00';
//        parse_str($str,$data);
//        var_dump($data);die;
        $status = $this->aop->rsaCheckV1($_POST, $this->config->payPublicCertPath, $this->aop->signType);
        if ($status) {
            $status = true;
        } else {
            $status = false;
        }
        return [
            'status' => $status,
            'payOrderId' => $_POST['out_trade_no'] ?? '', // order_id
            'trade_no' => $_POST['trade_no'] ?? '',
            'query_id' => $_POST['queryId'] ?? '',
            'data' => $_POST ?? [],
        ];
    }

    public function AntMerchantExpandIndirectImageUploadRequest($url, $type = '')
    {
        // 构造请求参数以调用接口
        $request = new AntMerchantExpandIndirectImageUploadRequest();

        // 设置图片二进制字节流
        $request->setImageContent('@' . $url);

        if ($type == '') {
            $arr = explode('.', $url);
            $type = end($arr);
        }
        // 设置图片格式
        $request->setImageType($type);
        $responseResult = $this->aop->execute($request);

        $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
        $resultCode = $responseResult->$responseNode->code;
        if (empty($resultCode) || $resultCode != 10000) {
            throw new AgilePaymentException($responseResult->$responseNode->sub_msg ?? '', 0, func_get_args());
        }
        Tool::log($responseResult, 'info', 'ZFT/aliUpdate');
        return $responseResult->$responseNode->image_id ?? '';

    }

    /**
     * @desc 解除二级商户
     * @param string $smid
     * @return object
     * @throws AgilePaymentException
     */
    public function AntMerchantExpandIndirectZftDeleteRequest(string $smid)
    {
        $data = new \stdClass();
        $data->smid = $smid;
        $request = new AntMerchantExpandIndirectZftDeleteRequest();
        $request->setBizContent(json_encode($data));
        $result = $this->aop->execute($request);

        $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
        $resultCode = $result->$responseNode->code;

        if (empty($resultCode) || $resultCode != 10000) {
            throw new AgilePaymentException($result->$responseNode->sub_msg ?? '作废失败！');
        }

        $resultData = [
            'status' => true,
            'msg' => $result->$responseNode->msg ?? ''
        ];
        return (object)$resultData;
    }

    /**
     * @desc 进件进度查询
     * @param string $externalId
     * @return object
     * @throws AgilePaymentException
     */
    public function AntMerchantExpandIndirectZftorderQueryRequest(string $externalId)
    {
        $data = new \stdClass();
        $data->external_id = $externalId;

        $request = new AntMerchantExpandIndirectZftorderQueryRequest();
        $request->setBizContent(json_encode($data));
        $result = $this->aop->execute($request);
        $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
        $resultCode = $result->$responseNode->code;

        if (empty($resultCode) || $resultCode != 10000) {
            throw new AgilePaymentException($result->$responseNode->sub_msg ?? '作废失败！');
        }
        $resultData = [
            'status' => true,
            'msg' => $result->$responseNode->msg ?? '',
            'data' => $result->$responseNode->orders ?? '',
        ];
        return (object)$resultData;
    }

    /**
     * @desc 阿里进件数据验证
     * @param $data
     * @return mixed
     */
    public function AntMerchantExpandIndirectZftConsultRequest($data)
    {
        $request = new AntMerchantExpandIndirectZftConsultRequest();
        $request->setBizContent(json_encode($data));
        $result = $this->aop->execute($request);
        Tool::log([$data,$result,$this->config],'info','ZFT/debug');
        $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
        $resultCode = $result->$responseNode->code;
        if (empty($resultCode) || $resultCode != 10000) {
            throw new AgilePaymentException($result->$responseNode->sub_msg ?? '核对失败！', 0, func_get_args());
        }

        $resultData = [
            'status' => true,
            'msg' => $result->$responseNode->msg ?? '',
            'sub_msg' => $result->$responseNode->sub_msg ?? '',
            'data' => [
                "order_id" => $result->$responseNode->order_id,
            ],
        ];
        return (object)$resultData;
    }


    /**
     * @desc 阿里进件
     * @param $data
     * @return mixed
     */
    public function AntMerchantExpandIndirectZftCreateRequest($data)
    {
        $request = new AntMerchantExpandIndirectZftCreateRequest();
        $request->setBizContent(json_encode($data));
        $result = $this->aop->execute($request);
        Tool::log([$data,$result],'info','ZFT/createRequest');
        $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
        $resultCode = $result->$responseNode->code;
        if (empty($resultCode) || $resultCode != 10000) {
            throw new AgilePaymentException($result->$responseNode->sub_msg ?? '核对失败！', 0, func_get_args());
        }
        $resultData = [
            'status' => true,
            'msg' => $result->$responseNode->msg ?? '',
            'sub_msg' => $result->$responseNode->sub_msg ?? '',
            'data' => [
                "order_id" => $result->$responseNode->order_id,
            ],
        ];
        return (object)$resultData;
    }

    /**
     * @desc 分账关系绑定
     * @return void
     */
    public function AlipayTradeRoyaltyRelationBindRequest(string $outOrderId, array $list)
    {
        if (count($list) > 20 || count($list) < 1) {
            throw new AgilePaymentException('列表个数错误！');
        }
        $data = new \stdClass();
        $data->out_request_no = $outOrderId;
        $data->receiver_list = $list;
        $request = new AlipayTradeRoyaltyRelationBindRequest();

        $request->setBizContent(json_encode($data));
        $result = $this->aop->execute($request);
        $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
        $resultCode = $result->$responseNode->code;
        if (empty($resultCode) || $resultCode != 10000) {
            throw new AgilePaymentException($result->$responseNode->sub_msg ?? '', 0, func_get_args());
        }
        return (object)[
            'status' => true,
            'msg' => $result->$responseNode->msg ?? '',
            'result_code' => $result->$responseNode->result_code,
        ];

    }


    public function AlipayTradeSettleConfirmRequest(string $outOrderId, string $tradeNo, array $settleInfo, bool $freeze = true)
    {
        $data = new \stdClass();
        $data->out_request_no = $outOrderId;
        $data->trade_no = $tradeNo;
        $data->settle_info = [
            'settle_detail_infos'   => [
                $settleInfo,
            ],
        ];

        $data->extend_params = [
            'royalty_freeze' => $freeze,
        ];

        $request = new AlipayTradeSettleConfirmRequest();
        $request->setBizContent(json_encode($data));
        $result = $this->aop->execute($request);
        Tool::log([$data,$result],'info','ZFT/AlipayTradeSettleConfirmRequest');
        $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
        $resultCode = $result->$responseNode->code;
        if (empty($resultCode) || $resultCode != 10000) {
            throw new AgilePaymentException($result->$responseNode->sub_msg ?? '', 0, func_get_args());
        }

        return (object)[
            'status' => true,
            'msg' => $result->$responseNode->msg ?? '',
            'data' => [
                'tradeNo' => $result->$responseNode->trade_no ?? '',
                'outRequestNo' => $result->$responseNode->out_request_no ?? '',
                'settleAmount' => $result->$responseNode->settle_amount ?? '',
            ]
        ];

    }

    /**
     * @desc 支付宝分账处理
     * @param string $outOrderId
     * @param string $tradeNo
     * @param string $transInLoginId 入账方支付宝登陆账号 电话或邮箱
     * @param string $amount 分账金额 默认小于30%
     * @param string $desc 入账说明
     * @param string $royaltyType transfer: 分账, replenish : 营销补差价
     * @param bool $freeze 分账之后是否解冻
     * @param string $royaltyScene [达人佣金、平台服务费、技术服务费、其他]
     * @return object
     * @throws AgilePaymentException
     */
    public function AlipayTradeOrderSettleRequest(string $outOrderId, string $tradeNo, string $transInLoginId, string $amount, string $desc = '', string $royaltyType = 'transfer', bool $freeze = true, string $royaltyScene = '平台服务费')
    {
        $data = new \stdClass();
        $data->out_request_no = $outOrderId;
        $data->trade_no = $tradeNo;

        if ($royaltyType == 'transfer') {
            if (empty($desc)){
                $desc = '分账';
            }else{
                $desc = '分账 : ' . $desc;
            }
        }else{
            if (empty($desc)){
                $desc = '营销补差价';
            }else{
                $desc = '营销补差价 : ' . $desc;
            }
        }

        $data->royalty_parameters = [
            [
                'royalty_type' => $royaltyType, //
                'trans_in_type' => 'loginName',
                'trans_in'  => $transInLoginId,
                'amount'    => $this->fenToYuan($amount),
                'desc'  => $desc,
                'royalty_scene' => $royaltyScene,
            ]
        ];
        $data->extend_params = [
            'royalty_finish'    => $freeze,
        ];
        $data->royalty_mode = 'async';

        $request = new AlipayTradeOrderSettleRequest();
        $request->setBizContent(json_encode($data));
        $result = $this->aop->execute($request);
        Tool::log([$data,$result],'info','ZFT/settle');
        $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
        $resultCode = $result->$responseNode->code;
        if (empty($resultCode) || $resultCode != 10000) {
            throw new AgilePaymentException($result->$responseNode->sub_msg ?? '', 0, func_get_args());
        }

        return (object)[
            'status' => true,
            'msg' => $result->$responseNode->msg ?? '',
            'data' => [
                'tradeNo' => $result->$responseNode->trade_no ?? '',
                'outRequestNo' => $result->$responseNode->out_request_no ?? '',
                'settleAmount' => $result->$responseNode->settle_amount ?? '',
            ]
        ];


    }

    public function AntMerchantExpandIndirectZftSettlementmodifyRequest(string $smid, string $loginId, string $imageId = '')
    {
        $data = new \stdClass();
        $data->smid = $smid;
        $data->default_settle_rule = [
            'default_settle_type'   => 'alipayAccount',
            'default_settle_target' => $loginId,
        ];
        $data->alipay_logon_id = $loginId;
        // $data->license_auth_letter_image = $imageId;

        $request = new AntMerchantExpandIndirectZftSettlementmodifyRequest();

        $request->setBizContent(json_encode($data));
        $result = $this->aop->execute($request);
        $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
        $resultCode = $result->$responseNode->code;
        if (empty($resultCode) || $resultCode != 10000) {
            throw new AgilePaymentException($result->$responseNode->sub_msg ?? '', 0, func_get_args());
        }
        return (object)[
            'status' => true,
            'msg' => $result->$responseNode->msg ?? '',
            'data' => [
                'orderId' => $result->$responseNode->order_id ?? '',
            ]
        ];

    }

    public function AntMerchantExpandIndirectZftModifyRequest($data)
    {
        $request = new AntMerchantExpandIndirectZftModifyRequest();
        $request->setBizContent(json_encode($data));
        $result = $this->aop->execute($request);
        Tool::log(['in'=>$data,'out'=>$result],'info', 'ZFT/changeSellte');
        $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
        $resultCode = $result->$responseNode->code;
        if (empty($resultCode) || $resultCode != 10000) {
            throw new AgilePaymentException($result->$responseNode->sub_msg ?? '', 0, func_get_args());
        }
        return (object)[
            'status' => true,
            'msg' => $result->$responseNode->msg ?? '',
            'data' => [
                'orderId' => $result->$responseNode->order_id ?? '',
            ]

        ];

    }

    /**
     * @desc 解除分账关系
     */
    public function AlipayTradeRoyaltyRelationUnbindRequest(string $name, string $account)
    {
        $data = new \stdClass();
        $data->out_request_no = Tool::createOrderNo('ALIMERUNB','27');


        $data->receiver_list = [
            [
                'name'  => $name,
                'type'  => 'loginName',
                'account'   => $account,
            ]
        ];

        $request = new AlipayTradeRoyaltyRelationUnbindRequest();
        $request->setBizContent(json_encode($data));
        $result = $this->aop->execute($request);
        Tool::log([$data,$result],'info','ZFT/merUnbind');
        $request = $this->aop->execute($request);
        $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
        $resultCode = $result->$responseNode->code;
        if (empty($resultCode) || $resultCode != 10000) {
            throw new AgilePaymentException($result->$responseNode->sub_msg ?? '', 0, func_get_args());
        }

        return (object)[
            'status' => true,
            'msg' => $result->$responseNode->sub_msg ?? '',
            'data' => [
                'resultCode' => $result->$responseNode->result_code,            ]
        ];

    }



    /**
     * @desc 获取分账关系列表
     */
    public function AlipayTradeRoyaltyRelationBatchqueryRequest(string $outOrderId, string $page, string $limit = '10')
    {
        if ($limit > 15){
            $limit = 20;
        }
        $data = new \stdClass();
        $data->outRequset_no = $outOrderId;
        $data->out_order_id = $outOrderId;
        $request = new AlipayTradeRoyaltyRelationBatchqueryRequest();


    }


    public function returnSuccess()
    {
        echo 'success';
    }
}
