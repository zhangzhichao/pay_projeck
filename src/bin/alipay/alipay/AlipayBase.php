<?php

namespace AgilePayments\bin\alipay\alipay;

use AgilePayments\bin\AgilePaymentLog;
use AgilePayments\Config;
use AgilePayments\bin\extend\alipay\AopCertClient;
use AgilePayments\bin\extend\alipay\AopClient;
use AgilePayments\Tool;

abstract class AlipayBase
{
    protected Factory $aliPayObj;

    public $config;

    public $aop;

    protected function __construct(Config $config)
    {
        $this->config = $config;
        $this->makeObj();
    }

    /**
     * @desc 生成工厂对象
     * @return void
     */
    private function makeObj()
    {
        if ($this->config->type == 1){
            $this->aop = new AopClient();
            $this->aop->alipayrsaPublicKey = $this->config->payPublicCertValue;
        }else{
            $this->aop = new AopCertClient();
            $this->aop->alipayrsaPublicKey = $this->aop->getPublicKey(Tool::getRootPath() . $this->config->payPublicCertPath);//调用getPublicKey从支付宝公钥证书中提取公钥
            $this->aop->isCheckAlipayPublicCert = true;//是否校验自动下载的支付宝公钥证书，如果开启校验要保证支付宝根证书在有效期内
            $this->aop->appCertSN = $this->aop->getCertSN(Tool::getRootPath() . $this->config->serverPublicCertPath);//调用getCertSN获取证书序列号
            $this->aop->alipayRootCertSN = $this->aop->getRootCertSN(Tool::getRootPath() . $this->config->payRootCertPath);//调用getRootCertSN获取支付宝根证书序列号
        }
        $this->aop->rsaPrivateKey = $this->config->serverPrivateCertValue;
        if ($this->config->isSendBox == 1){
            $this->aop->gatewayUrl = 'https://openapi-sandbox.dl.alipaydev.com/gateway.do';
        }else{
            $this->aop->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
        }
        $this->aop->appId =$this->config->appId;
        $this->aop->apiVersion = '1.0';
        $this->aop->signType = 'RSA2';
        $this->aop->postCharset = 'utf-8';
        $this->aop->format = 'json';
    }


    protected function yuanToFen(string $amount): string
    {
        $amount = bcmul($amount, '100') ?? '0';
        if ($amount < 0) {
            $amount = '0';
        }
        return $amount;
    }

    protected function fenToYuan(string $amount): string
    {
        $amount = bcdiv($amount, '100', 2) ?? '0.00';
        if ($amount < 0) {
            $amount = '0.00';
        }
        return $amount;
    }

}
