<?php

namespace AgilePayments\bin\alipay\alipay;

use AgilePayments\bin\extend\alipay\request\AlipayTradeAppPayRequest;
use AgilePayments\bin\extend\alipay\request\AlipayTradeWapPayRequest;
use AgilePayments\bin\format\PayReturn;
use AgilePayments\bin\AgilePaymentException;
use AgilePayments\Config;
use AgilePayments\Tool;
use AgilePayments\bin\extend\alipay\request\AlipayFundAuthOrderAppFreezeRequest;

class Pay extends AlipayBase
{
    public function __construct(Config $config)
    {
        parent::__construct($config);
    }

    /**
     * @desc alipay App 支付
     * @param string $outOrderId 支付订单编号
     * @param int $pay_price 支付价格（分）
     * @param string $out_time 过期时间
     * @param string $notify_url 回调地址
     * @param array $passback_params 扩展参数
     * @param string $title 商品名称
     * @return PayReturn
     */
    public function wap(string $outOrderId, int $pay_price, string $out_time, string $notify_url, string $passback_params = '', string $title = ''): PayReturn
    {
        try {
            $object = new \stdClass();
            $object->out_trade_no = $outOrderId;
            $object->total_amount = bcdiv($pay_price, 100, 2);
            $object->subject = $title ?? $outOrderId;
            $object->product_code = 'QUICK_WAP_WAY';
            $object->time_expire = date('Y-m-d H:i:s', $out_time);
            $baseUrl = Tool::env('UMFintechPay.notifyHost', 'https://testnftapi.feiyangyinxiang.cn/');
            $object->quit_url = $baseUrl . 'quit_url';
            $object->passback_params = $passback_params;
            $json = json_encode($object, JSON_UNESCAPED_SLASHES);
            $request = new AlipayTradeWapPayRequest();
            $request->setReturnUrl($baseUrl . 'return_url');
            $request->setNotifyUrl($notify_url);
            $request->setBizContent(str_replace('"', '\'', $json));
            $orderString = $this->aop->pageExecute($request, "POST");

            $status = true;
            $msg = 'success';
            $data['appPayRequest'] = $orderString;
            $data['pay_type'] = $this->config->payType;

        } catch (Exception $e) {
            throw new AgilePaymentException($e->getMessage(), 0, func_get_args());
        } catch (\Error $e) {
            throw new AgilePaymentException($e->getMessage(), 0, func_get_args());
        }

        return new PayReturn($data, $status, $msg);
    }

    /**
     * @desc alipay App 支付
     * @param string $outOrderId 支付订单编号
     * @param int $pay_price 支付价格（分）
     * @param string $out_time 过期时间
     * @param string $notify_url 回调地址
     * @param array $passback_params 扩展参数
     * @param string $title 商品名称
     * @return PayReturn
     */
    public function app(string $outOrderId, int $pay_price, string $out_time, string $notify_url, string $passback_params = '', string $title = ''): PayReturn
    {
        try {
            $object = new \stdClass();
            $object->out_trade_no = $outOrderId;
            $object->total_amount = bcdiv($pay_price, 100, 2);
            $object->subject = $title ?? $outOrderId;
            $object->product_code = 'QUICK_MSECURITY_PAY';
            $object->time_expire = date('Y-m-d H:i:s', $out_time);
            $object->passback_params = $passback_params;
            $json = json_encode($object);
            $request = new AlipayTradeAppPayRequest();
            $request->setNotifyUrl($notify_url);
            $request->setBizContent($json);
            $orderString = $this->aop->sdkExecute($request);

            $status = true;
            $msg = 'success';
            $data['appPayRequest'] = $orderString;
            $data['pay_type'] = $this->config->payType;

        } catch (Exception $e) {
            throw new AgilePaymentException($e->getMessage(), 0, func_get_args());
        } catch (\Error $e) {
            throw new AgilePaymentException($e->getMessage(), 0, func_get_args());
        }

        return new PayReturn($data, $status, $msg);

    }


    /**
     * @desc alipay freeze 支付
     * @param string $outOrderId 支付订单编号
     * @param int $pay_price 支付价格（分）
     * @param string $out_time 过期时间
     * @param string $notify_url 回调地址
     * @param array $extra_param 扩展参数
     * @param string $title 商品名称
     * @return PayReturn
     */
    public function freeze(string $outOrderId, int $pay_price, string $out_time, string $notify_url, string $passback_params = '', string $title = ''): PayReturn
    {
        try {
            $object = new \stdClass();
            $object->out_order_no = $outOrderId;
            $object->out_request_no = $outOrderId;
            $object->order_title = $title ?? $outOrderId;
            $object->amount = bcdiv($pay_price, 100, 2);
            $object->product_code = 'PREAUTH_PAY';
            $json = json_encode($object);
            $request = new AlipayFundAuthOrderAppFreezeRequest();
            $request->setNotifyUrl($notify_url);
            $request->setBizContent($json);
            $orderString = $this->aop->sdkExecute($request);
            $status = true;
            $msg = 'success';
            $data['appPayRequest'] = $orderString;
            $data['pay_type'] = $this->config->payType;

        } catch (Exception $e) {
            throw new AgilePaymentException($e->getMessage(), 0, func_get_args());
        } catch (\Error $e) {
            throw new AgilePaymentException($e->getMessage(), 0, func_get_args());
        }

        return new PayReturn($data, $status, $msg);

    }

    public function merapp(string $outOrderId, int $pay_price, string $out_time, string $notify_url, string $passback_params = '', string $title = '', string $merchantId = ''): PayReturn
    {
        try {
            if ($merchantId == ''){
                throw new AgilePaymentException('缺少必要参数!');
            }
            $object = new \stdClass();
            $object->out_trade_no = $outOrderId;
            $object->total_amount = bcdiv($pay_price, 100, 2);
            $object->subject = $title ?? $outOrderId;
            $object->product_code = 'QUICK_MSECURITY_PAY';
            $object->time_expire = date('Y-m-d H:i:s', $out_time);
            $object->settle_info = [
                'settle_period_time'   => '364d',
                'settle_detail_infos'  => [
                    [
                        'amount' => bcdiv($pay_price, 100, 2),
                        'trans_in_type' => 'defaultSettle',
                    ]
                ],

            ];
            $object->sub_merchant = [
                'merchant_id' => $merchantId,
            ];
            $object->extend_params = [
                'bank_memo' => $object->subject
            ];
            $object->passback_params = $passback_params;
            $json = json_encode($object);
            $request = new AlipayTradeAppPayRequest();
            $request->setNotifyUrl($notify_url);
            $request->setBizContent($json);
            $orderString = $this->aop->sdkExecute($request);

            $status = true;
            $msg = 'success';
            $data['appPayRequest'] = $orderString;
            $data['pay_type'] = $this->config->payType;

        } catch (Exception $e) {
            throw new AgilePaymentException($e->getMessage(), 0, func_get_args());
        } catch (\Error $e) {
            throw new AgilePaymentException($e->getMessage(), 0, func_get_args());
        }

        return new PayReturn($data, $status, $msg);

    }

}
