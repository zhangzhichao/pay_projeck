<?php

namespace AgilePayments\bin\format;

use AgilePayments\bin\AgilePaymentException;

final class QueryFormat extends ReturnFormat
{
    protected $date;

    protected $amount;

    protected $tradeNo;

    protected $outOrderId;

    protected $tradeStatus;

    public function __construct(array $data, bool $status = true, string $msg = 'success')
    {
        $this->setValue($data);
        $this->checkSelf();
        parent::__construct($data, $status, $msg);
    }

    private function setValue(array $data)
    {
        if (empty($data)) return;
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
    }

    private function checkSelf()
    {
        if (empty($this->tradeStatus)) {
            throw new AgilePaymentException('缺少必要返回值!');
        }
        $arr = [
            "date" => "2024-02-28 09:12:13",
            "amount" => "0.00",
            "tradeNo" => "2024022822001417971423756694",
            "outOrderId" => "TESTO5S9JKO373093368",
            "tradeStatus" => "TRADE_CLOSED"
        ];
    }
}