<?php

namespace AgilePayments\bin\format;

use AgilePayments\bin\AgilePaymentException;

final class PayReturn extends ReturnFormat
{
    public function __construct(array $data, bool $status = true, string $msg = 'success')
    {
        parent::__construct($data, $status, $msg);
        if (empty($this->data['appPayRequest'])){
            throw new AgilePaymentException('data 参数错误!', 0, func_get_args());
        }
    }
}