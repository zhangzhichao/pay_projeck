<?php

namespace AgilePayments\bin\format;

use AgilePayments\bin\AgilePaymentException;

class ReturnFormat
{
    public $status = true;
    public $msg = 'success';
    public $data = [];
    public $time;
    public function __construct(array $data, bool $status = true, string $msg = 'success')
    {
        $this->status = $status;
        $this->msg = $msg;
        $this->data = $data;
        $this->time = time();
    }

    public function getAttr($name = '')
    {
        try {
            return $this->data[$name];
        }catch (\Exception $exception){
            throw new AgilePaymentException($exception->getMessage());
        }catch (\Error $exception){
            throw new AgilePaymentException($exception->getMessage());
        }
    }
}