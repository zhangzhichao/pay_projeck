<?php

namespace AgilePayments\bin\format;

use AgilePayments\bin\AgilePaymentException;

final class RefundQueryFormat extends ReturnFormat
{
    protected $outOrderId;

    protected $refundNo;

    protected $refundAmount;

    protected $totalAmount;

    // 成功统一返回 REFUND_SUCCESS
    protected $refundStatus;

    protected $tradeNo;

    public function __construct(array $data, bool $status = true, string $msg = 'success')
    {
        $this->setValue($data);
        $this->checkSelf();
        parent::__construct($data, $status, $msg);
    }

    private function setValue(array $data)
    {
        if (empty($data)) return;
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
    }

    private function checkSelf()
    {
        if ($this->status && (empty($this->refundStatus) || empty($this->outOrderId) || empty($this->tradeNo))) {
            throw new AgilePaymentException('缺少必要返回值!');
        }
    }
}