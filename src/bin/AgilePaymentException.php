<?php

namespace AgilePayments\bin;

use Throwable;
use AgilePayments\Tool;

/**
 * AgilePayment所有异常出口
 */
class AgilePaymentException extends \Exception
{
    public $uuid = '';
    public $code;
    public $status;
    public $message = '';
    public $time;

    public function __construct($message = "", $code = 0, $data = [], Throwable $previous = null)
    {
        $this->uuid = $GLOBALS['agileuuid'] ?? Tool::generateUUIDv4();
        $this->code = $code;
        $this->status = $code != '1' ? false : true;
        $this->time = time();
        $this->writeLog($message, $data);
        if (is_string($message)) {
            $this->message = $message;
            parent::__construct($message, $code, $previous);
        } else {
            $this->message = $message->getMessage();
            parent::__construct($message->getMessage(), $code, $previous);
        }


    }

    /**
     * 添加异常日志
     * @param $message 异常消息
     * @return void
     */
    private function writeLog($message, array $param = [])
    {
        $msg = is_string($message) ? $message : $message->getMessage();
        $file = is_string($message) ? (parent::getFile() . ':' . parent::getLine()) : $message->getFile() . ':' . $message->getLine();
//        $tree = is_string($message) ? (parent::getTrace()) : $message->getTrace();
        Tool::log([
            'msg' => $msg,
            'param' => $param,
            'uuid' => $this->uuid,
            'file' => $file,
            'date' => date('Y-m-d H:i:s'),
            'tree' => parent::getTraceAsString(),
        ], 'Exception', 'AgilePaymentException');
    }

}
