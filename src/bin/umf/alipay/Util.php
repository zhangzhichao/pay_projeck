<?php

namespace AgilePayments\bin\umf\alipay;

use AgilePayments\bin\AgilePaymentException;
use AgilePayments\bin\extend\alipay\request\AlipaySystemOauthTokenRequest;
use AgilePayments\bin\extend\umf\api\MerAccessMemberBalance;
use AgilePayments\Config;
use AgilePayments\Tool;
use Alipay\EasySDK\Kernel\Util\ResponseChecker;

class Util extends AlipayBase
{
    public function __construct(Config $config)
    {
        parent::__construct($config);
    }

    /**
     * @desc 获取ali用户编号
     * @param string $code 授权码
     * @param bool $getOpenId 是否获取openId 兼容支付宝逐渐弃用userId
     * @return mixed|string
     * @throws \Exception
     */
    public function getAlipayUserId(string $code, bool $getOpenId = false)
    {
        $util = new \AgilePayments\bin\alipay\alipay\Util($this->config);

        return $util->getAlipayUserId($code);
    }

    public function MerAccessMemberBalance($user_id)
    {
        $request = new MerAccessMemberBalance();
        $request->user_id = $user_id;

        $result = $this->aop->execute($request);
        if ($result->meta->ret_code == '0000') {
            return $result;
        } else {
            throw new AgilePaymentException($result->meta->ret_msg ?? '', 0, func_get_args());
        }

    }

}