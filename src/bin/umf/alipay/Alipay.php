<?php

namespace AgilePayments\bin\umf\alipay;

use AgilePayments\bin\AgilePaymentException;
use AgilePayments\bin\AgilePaymentLog;
use AgilePayments\bin\extend\umf\api\MerAccessConsumeCloseOrder;
use AgilePayments\bin\extend\umf\api\MerAccessConsumeGenaralOrder;
use AgilePayments\bin\extend\umf\api\MerAccessConsumeOrderQuery;
use AgilePayments\bin\extend\umf\api\MerAccessRefundOrderInfo;
use AgilePayments\bin\extend\umf\api\MerAccessRefundRefundOrder;
use AgilePayments\bin\format\PayReturn;
use AgilePayments\bin\format\RefundQueryFormat;
use AgilePayments\bin\format\ReturnFormat;
use AgilePayments\bin\umf\alipay\AlipayBase;
use AgilePayments\bin\PayInterfaceClass;
use AgilePayments\bin\umf\Base;
use AgilePayments\Config;
use AgilePayments\Tool;

class Alipay extends Base implements PayInterfaceClass
{

    public function __construct(Config $config)
    {
        parent::__construct($config);
    }

    /**
     * 支付路由
     * @return PayReturn
     */
    public function pay(...$arge): PayReturn
    {
        $platform = strtolower($this->config->platform);
        $payment = strtolower($this->config->payment);
        $payType = strtolower($this->config->payType);
        $class = "\\AgilePayments\\bin\\{$platform}\\{$payment}\\Pay";
        if (!class_exists($class)) {
            throw new AgilePaymentException("调用类[$class]不存在！");
        }
        if (!method_exists($class, $payType)) {
            throw new AgilePaymentException("调用方法[$class::$payType()]不存在！");
        }
        $data = call_user_func_array([new $class($this->config), $payType], $arge);
        return $data;
    }

    /**
     * 退款
     * @param array $payInfo
     * @param int $price
     * @param string $refundReason
     * @return RefundQueryFormat
     * @throws AgilePaymentException
     */
    public function refund(array $payInfo, int $price, $notify = ''): RefundQueryFormat
    {
        $request = new MerAccessRefundRefundOrder();
        $request->order_id = $payInfo['refund_order_no'];
        $request->amount = $price;
        $request->notify_url = $notify;
        $request->ori_order_id = $payInfo['out_order_id'];
        $request->ori_mer_date = date('Ymd', Tool::analysisOrderNoTime($payInfo['out_order_id']));
        $request->refund_cmd = $payInfo['split'];

        if (!in_array($result->meta->ret_code, ['0000', '00462624'])) {
            throw new AgilePaymentException($result->meta->ret_msg ?? '', 0, func_get_args());
        }
        $status = true;
        $refundStatus = 'REFUND_SUCCESS';
        $msg = $result->meta->ret_msg ?? '';
        $data = [
            'refundStatus' => $refundStatus,
            'outOrderId' => $result->data->ori_order_id,
            'tradeNo' => $result->data->trade_no,
            'refundAmount' => $price,
            'totalAmount' => 'notFound',
        ];
        return new RefundQueryFormat ($data, $status, $msg);
    }

    /**
     * 关闭订单
     * @param string $outOrderId 原支付订单号
     * @param string $time 订单创建时间
     * @return ReturnFormat
     * @throws AgilePaymentException
     */
    public function close(string $outOrderId, string $time = ''): ReturnFormat
    {
        $request = new MerAccessConsumeCloseOrder();
        $request->order_id = $outOrderId;
        $request->mer_date = date('Ymd', $time);

        $result = $this->aop->execute($request);

        if ($result->meta->ret_code != '0000') {

            throw new AgilePaymentException($result->meta->ret_msg ?? '', 0, func_get_args());
        }
        $status = true;
        $msg = $result->meta->ret_msg ?? '';
        $data = [
            'outOrderId' => $result->data->order_id??'',
            'tradeNo' => $result->data->trade_no??'',
        ];
        return new ReturnFormat($data, $status, $msg);
    }


    /**
     * 订单查询
     * @param string $outOrderId 支付订单号
     * @return ReturnFormat
     * @throws AgilePaymentException
     */
    public function query(string $outOrderId, string $time): ReturnFormat
    {

        $request = new MerAccessConsumeOrderQuery();
        $request->order_id = $outOrderId;
        $request->mer_date = date('Ymd', $time);

        $result = $this->aop->execute($request);

        if ($result->meta->ret_code != '0000') {

            throw new AgilePaymentException($result->meta->ret_msg ?? '', 0, func_get_args());
        }
        $status = true;
        $msg = $result->meta->ret_msg ?? '';

        $tradeStatusArr = [
            0 => 'WAIT_BUYER_PAY', // 初始
            1 => 'WAIT_BUYER_PAY', // 支付中
            2 => 'TRADE_CLOSED', // 支付失败
            3 => 'TRADE_SUCCESS', // 冻结中
            4 => 'TRADE_FINISHED', // 完成
            5 => 'TRADE_CLOSED', // 已退费
            6 => 'TRADE_CLOSED', // 已关闭
        ];

        $date = \DateTime::createFromFormat('YmdHis', $result->data->mer_date . $result->data->creat_time);
        $formattedDate = $date->format('Y-m-d H:i:s');

        $data['data'] = [
            "date" => $formattedDate,
            "amount" => $result->data->amount,
            "tradeNo" => $result->data->trade_no,
            "outOrderId" => $result->data->order_id,
            "tradeStatus" => $tradeStatusArr[$result->data->order_state] ?? 'UNDEFINED',
        ];

        return new ReturnFormat($data, $status, $msg);
    }

    /**
     * @param string $refundNo 退款订单号
     * @param string $outOrderId 支付订单号
     * @return RefundQueryFormat
     * @throws AgilePaymentException
     */
    public function refundQuery(string $refundNo, string $outOrderId): RefundQueryFormat
    {
        $request = new MerAccessRefundOrderInfo();
        $request->order_id = $refundNo;
        $request->mer_date = date('Ymd', Tool::analysisOrderNoTime($refundNo));
        $result = $this->aop->execute($request);


        if ($result->meta->ret_code != '0000') {

            throw new AgilePaymentException($result->meta->ret_msg ?? '', 0, func_get_args());
        }
        $status = false;
        if (!empty($result->data->trade_state) && $result->data->trade_state == '1') {
            $status = true;
        }
        $msg = $result->meta->ret_msg ?? '';
        $refundStatusArr = [
            0 => 'REFUND_INIT', //初始
            1 => 'REFUND_SUCCESS', //成功
            2 => 'REFUND_FAIL', //失败
            3 => 'REFUND_PROCESSING', // 处理中
        ];
        $data = [
            'refundStatus' => $refundStatusArr[$result->data->trade_state] ?? 'UNDEFINED',
            'outOrderId' => $result->data->ori_order_id ?? '',
            'refundNo' => $result->data->order_id ?? '',
            'tradeNo' => $result->data->trade_no ?? '',
            'refundAmount' => '',
            'totalAmount' => '',
        ];
        return new RefundQueryFormat($data, $status, $msg);
    }

    public function returnSuccess()
    {
        echo '{"meta":{"ret_code":"0000"}}';
    }

    public function __call($name, $arguments)
    {
        throw new AgilePaymentException("调用方法[" . self::class . ":{$name}()]不存在！");
    }

    public function UMFCreatePayOrder(string $outOrderId, int $pay_price, array $splitData)
    {
        $request = new MerAccessConsumeGenaralOrder();
        $request->order_id = $outOrderId;
        $request->amount = $pay_price;
        $request->split_cmd = $splitData;
        $result = $this->aop->execute($request);

        if ($result->meta->ret_code == '0000') {
            return json_decode(json_encode([
                'status' => true,
                'msg' => $result->meta->ret_msg ?? '',
                'data' => ['appPayRequest' => $result->data ?? [], 'pay_type' => ''],
            ]));
        } else {
            $msg = $result->meta->ret_msg ?? '';
            throw new AgilePaymentException($msg, 0, func_get_args());
        }

    }


    public function verifySign()
    {
        $sign = $_SERVER['HTTP_SIGNATURE']??'';
        $body = file_get_contents('php://input')??'';
        $plan = explode('&',$body)[1]??'';
        if ($this->aop->verifySign($plan, $sign)){  // 验签成功！
            $status = true;
        }else{
            $status = false;
        }
        $payOrderId = json_decode($plan,true)['data']['order_id'];//substr(json_decode($plan,true)['data']['order_id'],0,-4);
        return [
            'code'          => json_decode($plan,true)['meta']['ret_code']??'',
            'status'        => $status,
            'payOrderId'    => $payOrderId, // order_id
            'data'          => json_decode($plan,true)['data']??[],
            'trade_no'      => json_decode($plan,true)['data']['trade_no']??'',
            'query_id'      => json_decode($plan,true)['data']['queryId']??'',
        ];
    }
}
