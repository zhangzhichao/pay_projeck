<?php

namespace AgilePayments\bin\umf\alipay;

use AgilePayments\bin\extend\umf\api\MerAccessPaymentWechatPlatformPay;
use AgilePayments\bin\format\PayReturn;
use AgilePayments\bin\AgilePaymentException;
use AgilePayments\bin\umf\Base;
use AgilePayments\Config;
use AgilePayments\Tool;

class Pay extends Base
{
    public function __construct(Config $config)
    {
        parent::__construct($config);
    }

    public function mini(string $outOrderId, int $pay_price, string $out_time, string $notify_url, string $passback_params = '', string $title = '', string $openId = '', string $tradeNo = ''): PayReturn
    {

        $request = new MerAccessPaymentWechatPlatformPay();

        $request->trade_no = $tradeNo;
        $request->notify_url = $notify_url;
        $request->mer_trace = $outOrderId . '_pay';
        $request->consumer_id = $outOrderId . '_pay';
        $request->openid     = $openId;
        $request->appid     = $this->config->appId;
        $request->amount     = $pay_price;
        $request->goods_inf = $outOrderId;
        $request->remark = $outOrderId;

        $result = $this->aop->execute($request);

        if ($result->meta->ret_code == '0000') {

            $status = true;
            $msg = 'success';
            $data['appPayRequest'] = $result->data ?? [];
            $data['pay_type'] = '';
        } else {
            throw new AgilePaymentException($result->meta->ret_msg ?? '', 0, func_get_args());
        }

        return new PayReturn($data, $status, $msg);
    }

}
