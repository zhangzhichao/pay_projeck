<?php

namespace AgilePayments\bin\umf;

use AgilePayments\bin\AgilePaymentException;
use AgilePayments\bin\extend\alipay\request\AlipaySystemOauthTokenRequest;
use AgilePayments\bin\extend\umf\api\MerAccessBankcardUnBindCard;
use AgilePayments\bin\extend\umf\api\MerAccessBindcardConfirm;
use AgilePayments\bin\extend\umf\api\MerAccessBindcardEnterprise;
use AgilePayments\bin\extend\umf\api\MerAccessBindcardOrder;
use AgilePayments\bin\extend\umf\api\MerAccessConsumeBalance;
use AgilePayments\bin\extend\umf\api\MerAccessConsumeFinishOrder;
use AgilePayments\bin\extend\umf\api\MerAccessConsumeGenaralOrder;
use AgilePayments\bin\extend\umf\api\MerAccessFileDownloadSettleFile;
use AgilePayments\bin\extend\umf\api\MerAccessFileUploadMaterials;
use AgilePayments\bin\extend\umf\api\MerAccessMemberBalance;
use AgilePayments\bin\extend\umf\api\MerAccessMemberBindCardInfo;
use AgilePayments\bin\extend\umf\api\MerAccessMemberMemberInfo;
use AgilePayments\bin\extend\umf\api\MerAccessMemberTradingQuery;
use AgilePayments\bin\extend\umf\api\MerAccessPaymentEbank;
use AgilePayments\bin\extend\umf\api\MerAccessPaymentQuickConfirm;
use AgilePayments\bin\extend\umf\api\MerAccessPaymentQuickOrder;
use AgilePayments\bin\extend\umf\api\MerAccessPaymentQuickVerifyCode;
use AgilePayments\bin\extend\umf\api\MerAccessRechargeOrder;
use AgilePayments\bin\extend\umf\api\MerAccessRegisterPersonal;
use AgilePayments\bin\extend\umf\api\MerAccessSubmerUpdate;
use AgilePayments\bin\extend\umf\api\MerAccessSubmerUpdateState;
use AgilePayments\bin\extend\umf\api\MerAccessWithdrawalBalanceWithdrawal;
use AgilePayments\bin\extend\umf\api\MerAccessWithdrawalOrderInfo;
use AgilePayments\bin\format\ReturnFormat;
use AgilePayments\Config;
use AgilePayments\Tool;
use Alipay\EasySDK\Kernel\Util\ResponseChecker;

class Util extends Base
{
    public function __construct(Config $config)
    {
        parent::__construct($config);
    }

    /**
     * @desc 获取ali用户编号
     * @param string $code 授权码
     * @param bool $getOpenId 是否获取openId 兼容支付宝逐渐弃用userId
     * @return mixed|string
     * @throws \Exception
     */
    public function getAlipayUserId(string $code, bool $getOpenId = false)
    {
        $util = new \AgilePayments\bin\alipay\alipay\Util($this->config);

        return $util->getAlipayUserId($code);
    }

    /**
     * @desc 查询余额
     * @param $user_id
     * @return mixed
     * @throws AgilePaymentException
     */
    public function MerAccessMemberBalance($user_id)
    {
        $request = new MerAccessMemberBalance();
        $request->user_id = $user_id;

        $result = $this->aop->execute($request);
        if ($result->meta->ret_code == '0000') {
            return $result;
        } else {
            throw new AgilePaymentException($result->meta->ret_msg ?? '', 0, func_get_args());
        }

    }

    /**
     * @desc 账户余额提现
     * @param string $outOrderId
     * @param string $userId
     * @param int $amount
     * @param string $remark
     * @param string $pAgreementId
     * @param string $notifyUrl
     * @param string $mobileId
     * @return ReturnFormat
     * @throws AgilePaymentException
     */
    public function MerAccessWithdrawalBalanceWithdrawal(string $outOrderId, string $userId, int $amount, string $remark, string $pAgreementId, string $notifyUrl = '', string $mobileId = ''): ReturnFormat
    {
        $request = new MerAccessWithdrawalBalanceWithdrawal();
        $request->order_id = $outOrderId;
        $request->user_id = $userId;
        $request->amount = $amount;
        $request->remark = $remark;
        $request->p_agreement_id = $pAgreementId;
        $request->notify_url = $notifyUrl;
        $request->mobile_id = $mobileId;

        $result = $this->aop->execute($request);
        if ($result->meta->ret_code != '0000') {
            throw new AgilePaymentException($result->meta->ret_msg ?? '', 0, func_get_args());
        }

        $status = true;
        $msg = $result->meta->msg ?? '';
        $data = [
            'outOrderId' => $result->data->order_id ?? '',
            'amount' => $result->data->amount ?? '',
            'comAmt' => $result->data->com_amount ?? '',
            'tradeNo' => $result->data->trade_no ?? '',
            'avlBal' => $result->data->avl_bal ?? '',
        ];
        return new ReturnFormat($data, $status, $msg);
    }


    public function MerAccessWithdrawalOrderInfo(string $outOrderId, string $time): ReturnFormat
    {
        $request = new MerAccessWithdrawalOrderInfo();
        $request->order_id = $outOrderId;
        $request->mer_date = date('Ymd', $time);
        $result = $this->aop->execute($request);

        if ($result->meta->ret_code != '0000') {
            throw new AgilePaymentException($result->meta->ret_msg ?? '', 0, func_get_args());
        }

        $tradeStateArr = [
            0 => 'INIT', // '初始',
            1 => 'SUCCESS', // '成功',
            2 => 'FAIL', // '失败',
            3 => 'PAYING', // '付款中',
            4 => 'REFUNDFINISH', // '退单不付完成',
        ];

        $tradeStateText = $tradeStateArr[$result->data->trade_state] ?? 'UNDEFINED';
        $status = false;
        if ($result->data->trade_state == 1) {
            $status = true;
        }
        $msg = $tradeStateText;
        $data = [
            'outOrderId' => $result->data->order_id ?? '',
            'merDate' => $result->data->mer_date ?? '',
            'amount' => $result->data->amount ?? '',
            'comAmt' => $result->data->com_amount ?? '',
            'tradeNo' => $result->data->trade_no ?? '',
            'avlBal' => $result->data->avl_bal ?? '',
            'tradeState' => $result->data->trade_state ?? '',
        ];
        return new ReturnFormat($data, $status, $msg);
    }

    /**
     * @deac 账户流水查询
     * @param $user_id int 联动侧user_id
     * @param $type int 查询类型 【1：子商户在途流水，2:子商户余额流水】
     * @param $page
     * @param $limit
     * @param $startDate string 查询时间范围（一年内，30天数据）
     * @param $endDate string 查询时间范围（一年内，30天数据）
     * @return ReturnFormat
     * @throws AgilePaymentException
     */
    public function MerAccessMemberTradingQuery($user_id, $type = '1', $page = 1, $limit = 10, $startDate = '', $endDate = ''): ReturnFormat
    {
        $startDate = $startDate != '' ? $startDate : date('Ymd');
        $endDate = $endDate != '' ? $endDate : date('Ymd');

        if ($type == 1) {
            $acc_type = '105';
        } else {
            $acc_type = '108';
        }

        $request = new MerAccessMemberTradingQuery();
        $request->user_id = $userId;
        $request->acc_type = $accType;
        $request->start_date = $startDate;
        $request->end_date = $endDate;
        $request->page_num = $pageNum;
        $request->page_size = $pageSize;

        $result = $this->aop->execute($request);

        if ($result->meta->ret_code != '0000') {
            throw new AgilePaymentException($result->meta->ret_msg ?? '', 0, func_get_args());
        }

        $status = true;
        $msg = $result->meta->msg ?? '';
        $data = (array)$result->data;
        return new ReturnFormat($data, $status, $msg);
    }

    /**
     * 生成结算对账文件
     * @param string $time 对账日期时间戳
     * @return ReturnFormat
     * @throws AgilePaymentException
     */
    public function MerAccessFileDownloadSettleFile($time): ReturnFormat
    {
        // todo 返回值不确定
        $request = new MerAccessFileDownloadSettleFile();
        $request->mer_date = date('Ymd', $time);

        $result = $this->aop->execute($request);

        if ($result->meta->ret_code != '0000') {
            throw new AgilePaymentException($result->meta->ret_msg ?? '', 0, func_get_args());
        }

        $status = true;
        $msg = $result->meta->msg ?? '';
        $data = (array)$result->data;

        return new ReturnFormat($data, $status, $msg);

    }

    /**
     * @desc 提交企业子商户基础信息
     * @param string $notifyUrl
     * @param string $outOrderId
     * @param string $subMerName
     * @param string $certId
     * @param string $subMerType
     * @param string $subMerAlias
     * @param string $certType
     * @param string $legalName
     * @param string $identityId
     * @param string $contactsName
     * @param string $mobileId
     * @param string $email
     * @param string $address
     * @return ReturnFormat
     * @throws AgilePaymentException
     */
    public function MerAccessRegisterSubmer(
        string $notifyUrl, string $outOrderId, string $subMerName, string $certId, string $subMerType, string $subMerAlias,
        string $certType, string $legalName, string $identityId, string $contactsName, string $mobileId, string $email = '',
        string $address = ''): ReturnFormat
    {
        $request = new MerAccessRegisterSubmer();
        $request->notify_url = $notifyUrl;
        $request->order_id = $outOrderId;
        $request->sub_mer_id = $outOrderId;
        $request->sub_mer_name = $subMerName;
        $request->sub_mer_type = $subMerType;
        $request->sub_mer_alias = $subMerAlias;
        $request->cert_type = $certType;
        $request->cert_id = $certId;
        $request->legal_name = $this->aop->encrypt($legalName);
        $request->identity_id = $this->aop->encrypt($identityId);
        $request->contacts_name = $this->aop->encrypt($contactsName);
        $request->mobile_id = $mobileId;
        $request->email = $email;
        $request->address = $address;

        $result = $this->aop->execute($request);

        if ($result->meta->ret_code != '0000') {
            throw new AgilePaymentException($result->meta->ret_msg ?? '', 0, func_get_args());
        }

        $status = true;
        $msg = $result->meta->msg ?? '';
        $data = [
            'outOrderId' => $result->data->order_id ?? '',
            'merDate' => $result->data->mer_date ?? '',
            'subMerId' => $result->data->sub_mer_id ?? '',
            'userId' => $result->data->user_id ?? '',
        ];

        return new ReturnFormat($data, $status, $msg);
    }


    /**
     * @desc 提交企业子商户资质文件
     * @param string $notify_url
     * @param string $user_id
     * @param string $user_type
     * @param string $zipPath
     * @param string $fileName
     * @return ReturnFormat
     * @throws AgilePaymentException
     */
    public function MerAccessFileUploadMaterials(string $notify_url, string $user_id, string $user_type, string $zipPath, string $fileName): ReturnFormat
    {
        // TODO  待验证
        $zipContens = file_get_contents($zipPath);
        $request = new MerAccessFileUploadMaterials();
        $request->notify_url = $notify_url;
        $request->user_id = $user_id;
        $request->user_type = $user_type;
        $request->md5_cipher = $zipContens;
        $result = $this->aop->executeFile($request, [
            'multipart' => [
                [
                    'name' => 'file',
                    'filename' => $fileName,
                    'contents' => $zipContens
                ],
            ],
        ]);

        if (!in_array($result->meta->ret_code,['0000','00462101','00462102'])) {
            throw new AgilePaymentException($result->meta->ret_msg ?? '', 0, func_get_args());
        }

        $status = $result->meta->ret_code;
        $msg = $result->meta->msg ?? '';
        $data = [
            'outOrderId' => $result->data->order_id ?? '',
            'merDate' => $result->data->mer_date ?? '',
            'subMerId' => $result->data->sub_mer_id ?? '',
            'userId' => $result->data->user_id ?? '',
        ];

        return new ReturnFormat($data, $status, $msg);

    }

    /**
     * @desc 子商户信息查询
     * @param string $subMerId
     * @return ReturnFormat
     * @throws AgilePaymentException
     */
    public function MerAccessMemberMemberInfo(string $subMerId): ReturnFormat
    {
        $request = new MerAccessMemberMemberInfo();
        $request->sub_mer_id = $subMerId;

        $result = $this->aop->execute($request);

        if ($result->meta->ret_code != '0000') {
            throw new AgilePaymentException($result->meta->ret_msg ?? '', 0, func_get_args());
        }

        $status = true;
        $msg = $result->meta->msg ?? '';
        $data = (array)$result->data;

        return new ReturnFormat($data, $status, $msg);

    }

    /**
     * @desc 子商户信息修改
     * @param string $userId
     * @param string $subMerType
     * @param string $subMerName
     * @param string $subMerAlias
     * @param string $legalName
     * @param string $identityId
     * @param string $contactsName
     * @param string $mobileId
     * @return ReturnFormat
     * @throws AgilePaymentException
     */
    public function MerAccessSubmerUpdate(string $userId, string $subMerType = '', string $subMerName = '', string $subMerAlias = '', string $legalName = '', string $identityId = '', string $contactsName = '', string $mobileId = ''): ReturnFormat
    {
        if ($subMerType == '' && $subMerName == '' && $subMerAlias == '' && $legalName == '' && $identityId == '' && $contactsName == '' && $mobileId == '') {
            throw new AgilePaymentException('参数不能同时为空！', 0, func_get_args());
        }

        $request = new MerAccessSubmerUpdate();

        $request->user_id = $userId;
        if ($subMerType != '') $request->sub_mer_type = $subMerType;
        if ($subMerName != '') $request->sub_mer_name = $subMerName;
        if ($subMerAlias != '') $request->sub_mer_alias = $subMerAlias;
        if ($legalName != '') $request->legal_name = $this->aop->encrypt($legalName);
        if ($identityId != '') $request->identity_id = $this->aop->encrypt($identityId);
        if ($contactsName != '') $request->contacts_name = $this->aop->encrypt($contactsName);
        if ($mobileId != '') $request->mobile_id = $mobileId;

        if ($result->meta->ret_code != '0000') {
            throw new AgilePaymentException($result->meta->ret_msg ?? '', 0, func_get_args());
        }

        $status = true;
        $msg = $result->meta->msg ?? '';
        $data = (array)$result->data;

        return new ReturnFormat($data, $status, $msg);
    }

    /**
     * @desc 子商户状态管理
     * @param string $subMerId
     * @param string $subMerType 子商户状态，1-开通 2-注销 3-暂停
     * @return ReturnFormat
     * @throws AgilePaymentException
     */
    public function MerAccessSubmerUpdateState(string $subMerId, string $subMerType): ReturnFormat
    {
        $request = new MerAccessSubmerUpdateState();

        $request->sub_mer_id = $subMerId;
        $request->sub_mer_state = $subMerType;

        $result = $this->aop->execute($request);

        if ($result->meta->ret_code != '0000') {
            throw new AgilePaymentException($result->meta->ret_msg ?? '', 0, func_get_args());
        }

        $subMerStateArr = [
            1 => 'OPEN', // '开通'
            2 => 'CANCEL', // '注销'
            3 => 'PAUSED', // '暂停'
        ];

        $status = true;
        $msg = $subMerStateArr[$result->date->sub_mer_state] ?? 'UNDEFINED';
        $data = [
            'userId' => $result->data->user_id ?? '',
            'subMerState' => $result->data->sub_mer_state ?? '',
        ];

        return new ReturnFormat($data, $status, $msg);

    }


    /**
     * 绑定企业对公结算账户
     * @param string $outOrderId
     * @param string $userId
     * @param string $bankAccountName
     * @param string $bankAccount
     * @param string $gateId
     * @param string $notifyUrl
     * @param string $mobileId
     * @param string $bankBranchName
     * @param string $bankBranchCode
     * @return ReturnFormat
     * @throws AgilePaymentException
     */
    public function MerAccessBindcardEnterprise(string $outOrderId, string $userId, string $bankAccountName, string $bankAccount, string $gateId, string $notifyUrl = '', string $mobileId = '', string $bankBranchName = '', string $bankBranchCode = ''): ReturnFormat
    {
        $request = new MerAccessBindcardEnterprise();
        $request->order_id = $outOrderId;
        $request->user_id = $userId;
        $request->bank_account_name = $this->aop->encrypt($bankAccountName);
        $request->bank_account = $this->aop->encrypt($bankAccount);
        $request->gate_id = $gateId;
        $request->notify_url = $notifyUrl;
        $request->mobile_id = $mobileId;
        $request->bank_branch_name = $bankBranchName;
        $request->bank_branch_code = $bankBranchCode;

        $result = $this->aop->execute($request);

        if ($result->meta->ret_code != '0000') {
            throw new AgilePaymentException($result->meta->ret_msg ?? '', 0, func_get_args());
        }

        $status = true;
        $msg = $result->meta->msg ?? '';
        $date = [
            'outOrderId' => $result->data->order_id ?? '',
            'userId' => $result->data->user_id ?? '',
            'pAgreementId' => $result->data->p_agreement_id ?? '',
            'lastFourCardid' => $result->data->last_for_cardi ?? '',
        ];

        return new ReturnFormat($status, $msg, $date);

    }


    /**
     * @desc 绑定法人结算银行卡
     * @param string $outOrderId
     * @param string $userId
     * @param string $userType 子商户类型：1（个人商户），2 （个体工商户），3（企业商户）
     * @param string $cardId
     * @param string $bankAccountName
     * @param string $bankMobileId
     * @param string $notifyUrl
     * @return ReturnFormat
     * @throws AgilePaymentException
     */
    public function MerAccessBindcardOrder(string $outOrderId, string $userId, string $userType, string $cardId, string $bankAccountName, string $bankMobileId, string $notifyUrl = ''): ReturnFormat
    {

        $request = new MerAccessBindcardOrder();
        $request->order_id = $outOrderId;
        $request->user_id = $userId;
        $request->user_type = $userType;
        $request->card_id = $this->aop->encrypt($cardId);
        $request->bank_account_name = $this->aop->encrypt($bankAccountName);
        $request->bank_mobile_id = $bankMobileId;
        $request->notify_url = $notifyUrl;

        $result = $this->aop->execute($request);

        if ($result->meta->ret_code != '0000') {
            throw new AgilePaymentException($result->meta->ret_msg ?? '', 0, func_get_args());
        }

        $status = true;
        $msg = $result->meta->msg ?? '';
        $data = [
            'userId' => $result->data->user_id ?? '',
            'tradeNo' => $result->data->trade_no ?? '',
            'gateId' => $result->data->gate_id ?? '',
            'lastFourCardId' => $result->data->last_four_cardid ?? '',
            'pAgreementId' => $result->data->p_agreement_id ?? '',
        ];

        return new ReturnFormat($data, $status, $msg);

    }

    /**
     * @desc 确认绑卡
     * @param string $userId
     * @param string $tradeNo
     * @param string $verifyCode
     * @return ReturnFormat
     * @throws AgilePaymentException
     */
    public function MerAccessBindcardConfirm(string $userId, string $tradeNo, string $verifyCode): ReturnFormat
    {
        $request = new MerAccessBindcardConfirm();
        $request->user_id = $userId;
        $request->trade_no = $tradeNo;
        $request->verify_code = $verifyCode;

        $result = $this->aop->execute($request);

        if ($result->meta->ret_code != '0000') {
            throw new AgilePaymentException($result->meta->ret_msg ?? '', 0, func_get_args());
        }

        $status = true;
        $msg = $result->meta->msg ?? '';
        $data = [
            'outOrderId' => $result->data->order_id ?? '',
            'tradeNo' => $result->data->trade_no ?? '',
            'userId' => $result->data->user_id ?? '',
            'gateId' => $result->data->gate_id ?? '',
            'pAgreementId' => $result->data->p_agreement_id ?? '',
            'lastFourCardId' => $result->data->last_four_cardid ?? '',
        ];
        return new ReturnFormat($data, $status, $msg);
    }

    /**
     * @desc 子商户绑卡信息查询接口
     * @param string $userId
     * @return ReturnFormat
     * @throws AgilePaymentException
     */
    public function MerAccessMemberBindCardInfo(string $userId): ReturnFormat
    {
        $request = new MerAccessMemberBindCardInfo();
        $request->user_id = $userId;
        $result = $this->aop->execute($request);

        if ($result->meta->ret_code != '0000') {
            throw new AgilePaymentException($result->meta->ret_msg ?? '', 0, func_get_args());
        }
        $status = true;
        $msg = $result->meta->msg ?? '';
        $data = (array)$result->data;

        return new ReturnFormat($data, $status, $msg);

    }

    /**
     * @desc 提现协议解绑
     * @param string $userId
     * @param string $outOrderId
     * @param string $userType 用户类型：1-个人；2-个体；3-企业
     * @param string $pAgreementId
     * @param string $lastFourCardid
     * @return ReturnFormat
     * @throws AgilePaymentException
     */
    public function MerAccessBankcardUnBindCard(string $userId, string $outOrderId, string $userType, string $pAgreementId, string $lastFourCardid): ReturnFormat
    {
        $request = new MerAccessBankcardUnBindCard();
        $request->user_id = $userId;
        $request->order_id = $outOrderId;
        $request->user_type = $userType;
        $request->p_agreement_id = $pAgreementId;
        $request->last_four_cardid = $lastFourCardid;

        $result = $this->aop->execute($request);

        if ($result->meta->ret_code != '0000') {
            throw new AgilePaymentException($result->meta->ret_msg ?? '', 0, func_get_args());
        }

        $status = true;
        $msg = $result->meta->msg ?? '';
        $data = [
            'outOrderId' => $result->data->order_id ?? '',
            'userId' => $result->data->user_id ?? '',
            'userType' => $userType,
            'state' => $result->data->state ?? '',
            'pAgreementId' => $result->data->p_agreement_id ?? '',
            'lastFourCardId' => $result->data->last_four_cardid ?? '',
        ];

        return new ReturnFormat($data, $status, $msg);

    }

    /**
     * @deac 余额支付
     * @param string $outOrderId
     * @param string $tradeNo
     * @param string $amount
     * @param string $remark
     * @param string $notifyUrl
     * @param string $type 支付账号 【1：结算用户，2：分佣账号】
     * @param string $umfUserId 支付账号编号
     * @return ReturnFormat
     * @throws AgilePaymentException
     */
    public function MerAccessConsumeBalance(string $outOrderId, string $tradeNo, string $amount, string $remark = '', string $notifyUrl = '', string $type = '1', string $umfUserId = ''): ReturnFormat
    {
        if ($type == '1') {
            $user_id = Tool::env('UMFintechPay.settUserId');
        } else {
            $user_id = Tool::env('UMFintechPay.commUserId');
        }

        if ($umfUserId != '') {
            $user_id = $umfUserId;
        }

        $request = new MerAccessConsumeBalance();
        $request->notify_url = $notifyUrl;
        $request->trade_no = $tradeNo;
        $request->mer_trace = $outOrderId . '_pay';
        $request->user_id = $user_id;
        $request->amount = $amount;
        $request->remark = $remark;

        $result = $this->aop->execute($request);

        if ($result->meta->ret_code != '0000') {
            throw new AgilePaymentException($result->meta->ret_msg ?? '', 0, func_get_args());
        }

        $status = true;
        $msg = $result->meta->msg ?? '';
        $data = (array)$result->data;

        return new ReturnFormat($data, $status, $msg);

    }


    /**
     * @desc 银行卡支付信息（接口一）
     * @param string $tradeNo
     * @param string $outOrderId
     * @param int $amount
     * @param string $cardId
     * @param string $notifyUrl
     * @param string $pAgreementId
     * @return ReturnFormat
     * @throws AgilePaymentException
     */
    public function MerAccessPaymentQuickOrder(string $tradeNo, string $outOrderId, int $amount, string $cardId, string $notifyUrl, string $pAgreementId = ''): ReturnFormat
    {
        $user_id = Tool::env('UMFintechPay.settUserId');
        $request = new MerAccessPaymentQuickOrder();
        $request->notify_url = $notifyUrl;
        $request->trade_no = $tradeNo;
        $request->mer_cust_id = $user_id;
        $request->mer_trace = $outOrderId . '_pay';
        $request->amount = $amount;
        if ($pAgreementId != '') {
            $request->p_agreement_id = $pAgreementId;
        } else {
            $request->card_id = $this->aop->encrypt($cardId);
        }

        $result = $this->aop->execute($request);

        if ($result->meta->ret_code != '0000') {
            throw new AgilePaymentException($result->meta->ret_msg ?? '', 0, func_get_args());
        }
        $status = true;
        $msg = $result->meta->msg ?? '';
        $data = [
            'tradeNo' => $result->data->trade_no ?? '',
            'merTrace' => $result->data->mer_trace ?? '',
            'cardType' => $result->data->card_type ?? '',
            'gateId' => $result->data->gate_id ?? '',
            'payElements' => $result->data->pay_elements ?? '',
        ];

        return new ReturnFormat($data, $status, $msg);
    }


    /**
     * @desc 获取银行卡支付验证码
     * @param string $notifyUrl
     * @param string $tradeNo
     * @param string $merTrace
     * @param string $mobile
     * @param string $cardId
     * @param string $cardHolder
     * @param string $identityCode
     * @param string $validDate
     * @param string $cvv2
     * @param string $pAgreementId
     * @return ReturnFormat
     * @throws AgilePaymentException
     */
    public function MerAccessPaymentQuickVerifyCode(string $notifyUrl, string $tradeNo, string $merTrace, string $mobile, string $cardId, string $cardHolder, string $identityCode, string $validDate, string $cvv2, string $pAgreementId = ''): ReturnFormat
    {
        $request = new MerAccessPaymentQuickVerifyCode();
        $request->notify_url = $notifyUrl;
        $request->trade_no = $tradeNo;
        $request->mer_trace = $merTrace;
        $request->bank_mobile_id = $mobile;

        if ($pAgreementId != '') {
            $request->p_agreement_id = $pAgreementId;
        } else {
            $request->card_id = $this->aop->encrypt($cardId);
            $request->card_holder = $this->aop->encrypt($cardHolder);
            $request->identity_type = 1;
            $request->identity_code = $this->aop->encrypt($identityCode);
            $request->valid_date = $this->aop->encrypt($validDate);
            $request->cvv2 = $this->aop->encrypt($cvv2);
        }

        $result = $this->aop->execute($request);

        if ($result->meta->ret_code != '0000') {
            throw new AgilePaymentException($result->meta->ret_msg ?? '', 0, func_get_args());
        }

        $status = true;
        $msg = $result->meta->msg ?? '';
        $data = [
            'tradeNo' => $result->data->trade_no ?? '',
            'merTrace' => $result->data->mer_trace ?? '',
        ];

        return new ReturnFormat($data, $status, $msg);
    }


    /**
     * @desc 银行卡支付-确认支付
     * @param int $amount
     * @param string $VerifyCode
     * @param string $notifyUrl
     * @param string $tradeNo
     * @param string $merTrace
     * @param string $mobile
     * @param string $cardId
     * @param string $cardHolder
     * @param string $identityCode
     * @param string $validDate
     * @param string $cvv2
     * @param string $pAgreementId
     * @return ReturnFormat
     * @throws AgilePaymentException
     */
    public function MerAccessPaymentQuickConfirm(int $amount, string $VerifyCode, string $notifyUrl, string $tradeNo, string $merTrace, string $mobile, string $cardId, string $cardHolder, string $identityCode, string $validDate, string $cvv2, string $pAgreementId = ''): ReturnFormat
    {
        $request = new MerAccessPaymentQuickConfirm();
        $request->notify_url = $notifyUrl;
        $request->trade_no = $tradeNo;
        $request->mer_trace = $merTrace;
        $request->amount = $amount;
        $request->bank_mobile_id = $mobile;
        $request->verify_code = $VerifyCode;

        if ($pAgreementId != '') {
            $request->p_agreement_id = $pAgreementId;
        } else {
            $request->card_id = $this->aop->encrypt($cardId);
            $request->card_holder = $this->aop->encrypt($cardHolder);
            $request->identity_code = $this->aop->encrypt($identityCode);
            $request->valid_date = $this->aop->encrypt($validDate);
            $request->cvv2 = $this->aop->encrypt($cvv2);
        }

        $result = $this->aop->execute($request);

        if ($result->meta->ret_code != '0000') {
            throw new AgilePaymentException($result->meta->ret_msg ?? '', 0, func_get_args());
        }
        $status = true;
        $msg = $result->meta->msg ?? '';
        $data = [
            'tradeNo' => $result->data->trade_no ?? '',
            'merTrace' => $result->data->mer_trace ?? '',
            'amount' => $result->data->amount ?? '',
            'settleAmt' => $result->data->settle_amt ?? '',
            'pAgreementId' => $result->data->p_agreement_id ?? '',
            'gateId' => $result->data->gate_id ?? '',
            'lastFourCardId' => $result->data->last_four_cardid ?? '',
        ];

        return new ReturnFormat($data, $status, $msg);
    }


    /**
     * @desc 联动网银支付
     * @param int $amount
     * @param string $NotifyUrl
     * @param string $RetUrl
     * @param string $tradeNo
     * @param string $outOrderId
     * @param string $payType
     * @param string $gateId
     * @param string $expireTime
     * @return ReturnFormat
     * @throws AgilePaymentException
     */
    public function MerAccessPaymentEbank(int $amount, string $NotifyUrl, string $RetUrl, string $tradeNo, string $outOrderId, string $payType, string $gateId, string $expireTime): ReturnFormat
    {
        $request = new MerAccessPaymentEbank();
        $request->notify_url = $NotifyUrl;
        $request->ret_url = $RetUrl;
        $request->trade_no = $tradeNo;
        $request->mer_trace = $outOrderId . '_pay';
        $request->pay_type = $payType;
        $request->gate_id = $gateId;
        $request->amount = $amount;
        $request->expire_time = $expireTime;
        $data = [];
        $result = $this->aop->execute($request, ['timeout' => 2, 'allow_redirects' => false, 'on_stats' => function (\GuzzleHttp\TransferStats $stats) use (&$data) {
            if ($stats->getResponse()->getStatusCode() == 302) {
                $location = $stats->getResponse()->getHeader('Location')[0] ?? '';
                $data = [
                    'location' => $location ?? '',
                ];
            }
        }]);
        
        $status = true;
        $msg = 'HTTP 302';

        return new ReturnFormat($data, $status, $msg);

    }


    /**
     * @desc 子账户充值
     * @param string $userId
     * @param int $amount
     * @param string $outOrderId
     * @param string $notifyUrl
     * @return ReturnFormat
     * @throws AgilePaymentException
     */
    public function MerAccessRechargeOrder(string $userId, int $amount, string $outOrderId, string $notifyUrl): ReturnFormat
    {
        $request = new MerAccessRechargeOrder();
        $request->order_id = $outOrderId;
        $request->amount = $amount;
        $request->in_user_id = $userId;
        $request->notify_url = $notifyUrl;

        $result = $this->aop->execute($request);

        if ($result->meta->ret_code != '0000') {
            throw new AgilePaymentException($result->meta->ret_msg ?? '', 0, func_get_args());
        }

        $status = true;
        $msg = $result->meta->msg ?? '';
        $data = [
            'tradeNo' => $result->data->trade_no ?? '',
            'outOrderId' => $result->data->order_id ?? '',
        ];

        return new ReturnFormat($data, $status, $msg);
    }

    /**
     * @desc 订单解冻
     * @param string $tradeNo
     * @param string $notifyUrl
     * @return ReturnFormat
     * @throws AgilePaymentException
     */
    public function MerAccessConsumeFinishOrder(string $tradeNo, string $notifyUrl = ''): ReturnFormat
    {
        $request = new MerAccessConsumeFinishOrder();
        $request->trade_no = $tradeNo;
        $request->notify_url = $notifyUrl;

        $result = $this->aop->execute($request);

        if ($result->meta->ret_code != '0000') {
            throw new AgilePaymentException($result->meta->ret_msg ?? '', 0, func_get_args());
        }

        $status = true;
        $msg = $result->meta->msg ?? '';
        $data = [
            'tradeNo' => $result->data->trade_no ?? '',
        ];

        return new ReturnFormat($data, $status, $msg);
        
    }


    public function MerAccessConsumeGenaralOrder(string $outOrderId, int $amount, array $split): ReturnFormat
    {
        $request = new MerAccessConsumeGenaralOrder();
        $request->order_id = $outOrderId;
        $request->amount = $amount;
        $request->split_cmd = $split;

        $result = $this->aop->execute($request);

        if ($result->meta->ret_code != '0000') {
            throw new AgilePaymentException($result->meta->ret_msg ?? '', 0, func_get_args());
        }

        $status = true;
        $msg = $result->meta->msg ?? '';
        $data = [
            'appPayRequest' => $result->data??'',
            'pay_type' => $this->config->payType,
        ];

        return new ReturnFormat($data, $status, $msg);

    }


    /////////////////////////////////////////////////////////  test function   /////////////////////////////////////////

    public function MerAccessRegisterPersonal(string $outOrderId, string $merCustId, string $mobileId, string $custName, string $identityCode, string $subMerAlias, string $notifyUrl)
    {
        $request = new MerAccessRegisterPersonal();
        $request->notify_url = $notifyUrl;
        $request->order_id = $outOrderId;
        $request->mobile_id = $mobileId;
        $request->mer_cust_id = $merCustId;
        $request->cust_name = $this->aop->encrypt($custName);
        $request->identity_code = $this->aop->encrypt($identityCode);
        $request->sub_mer_alias = $subMerAlias;

        $result = $this->aop->execute($request);

        if ($result->meta->ret_code == '0000') {
            return $result;
        } else {
            throw new AgilePaymentException($result->meta->ret_msg ?? '', 0, func_get_args());
        }
    }

}