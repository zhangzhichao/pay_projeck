<?php

namespace AgilePayments\bin\umf;

use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use AgilePayments\bin\AgilePaymentLog;
use AgilePayments\Config;
use AgilePayments\Tool;
use AgilePayments\bin\extend\umf\Aop;

abstract class Base
{

    public $config;

    public Aop $aop;

    protected function __construct(Config $config)
    {
        $this->config = $config;
        $this->makeObj();
    }

    /**
     * @desc 生成工厂对象
     * @return void
     */
    private function makeObj()
    {
        $this->aop = new Aop($this->config);
    }


    protected function yuanToFen(string $amount): string
    {
        $amount = bcmul($amount, '100') ?? '0';
        if ($amount < 0) {
            $amount = '0';
        }
        return $amount;
    }

    protected function fenToYuan(string $amount): string
    {
        $amount = bcdiv($amount, '100', 2) ?? '0.00';
        if ($amount < 0) {
            $amount = '0.00';
        }
        return $amount;
    }

}