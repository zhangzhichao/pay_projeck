<?php

namespace AgilePayments\bin\extend\umf\api;

/**
 * 生成结算对账文件
 * @param string $mer_date 对账日期，格式为YYYYMMDD
 */
class MerAccessFileDownloadSettleFile
{
    public $data;

    protected $mer_id;
    protected $version = '1.0';
    protected $method = 'GET';
    
    public $mer_date;


    public function getUrl()
    {
        return 'merAccess/file/download/settleFile';
    }

    public function getData()
    {
        return [
            'mer_id'        => $this->mer_id,
            'version'       => '1.0',
            'mer_date'      => $this->mer_date,
        ];
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}