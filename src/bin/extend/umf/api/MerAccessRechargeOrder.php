<?php

namespace AgilePayments\bin\extend\umf\api;

/**
 * 子账户充值
 */
class MerAccessRechargeOrder
{
    public $data;

    protected $mer_id;
    protected $version = '1.0';
    protected $method = 'POST';

    public $order_id;
    public $amount;
    public $in_user_id;
    public $notify_url;


    public function getUrl()
    {
        return 'merAccess/recharge/order';
    }

    public function getData()
    {
        return [
            'mer_id' => $this->mer_id,
            'version' => $this->version,
            'mer_date' => date('Ymd'),
            'order_id' => $this->order_id,
            'amount' => $this->amount,
            'in_user_id' => $this->in_user_id,
            'notify_url' => $this->notify_url,
        ];
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}