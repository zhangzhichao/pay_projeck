<?php

namespace AgilePayments\bin\extend\umf\api;

/**
 * 绑定法人结算银行卡
 */
class MerAccessBindcardOrder
{
    public $data;

    protected $mer_id;
    protected $version = '1.1';
    protected $method = 'POST';

    public $order_id;
    public $user_id;
    public $user_type;
    public $card_id;
    public $bank_account_name;
    public $bank_mobile_id;
    public $notify_url;



    public function getUrl()
    {
        return 'merAccess/bindcard/order';
    }

    public function getData()
    {
        return [
            'mer_id'	        => $this->mer_id,
            'version'	        => $this->version,
            'order_id'	        => $this->order_id,
            'mer_date'	        => date('Ymd'),
            'user_id'	        => $this->user_id,
            'user_type'	        => $this->user_type,// 子商户类型：1（个人商户），2 （个体工商户），3（企业商户）
            'card_id'	        => $this->card_id,
            'bank_account_name'	=> $this->bank_account_name,
            'bank_mobile_id'	=> $this->bank_mobile_id,
            'notify_url'	    => $this->notify_url,
        ];
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}