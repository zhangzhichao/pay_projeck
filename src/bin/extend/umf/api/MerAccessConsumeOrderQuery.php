<?php

namespace AgilePayments\bin\extend\umf\api;

/**
 * 绑定企业对公结算账户
 */
class MerAccessConsumeOrderQuery
{
    public $data;

    protected $mer_id;
    protected $version = '1.0';
    protected $method = 'POST';

    public $order_id;
    public $mer_date;

    public function getUrl()
    {
        return 'merAccess/consume/orderQuery';
    }

    public function getData()
    {
        return [
            'mer_id'	=> $this->mer_id,
            'version'	=> $this->version,
            'order_id'	=> $this->order_id,
            'mer_date'	=> $this->mer_date,
        ];
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}