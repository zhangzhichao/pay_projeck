<?php

namespace AgilePayments\bin\extend\umf\api;

/**
 * 联动网银支付
 */
class MerAccessPaymentEbank
{
    public $data;

    protected $mer_id;
    protected $version = '1.0';
    protected $method = 'POST';

    public $notify_url;
    public $ret_url;
    public $trade_no;
    public $mer_trace;
    public $pay_type;
    public $gate_id;
    public $amount;
    public $expire_time;


    public function getUrl()
    {
        return 'merAccess/payment/ebank';
    }

    public function getData()
    {
        return [
            'mer_id' => $this->mer_id,
            'version' => $this->version,
            'notify_url' => $this->notify_url,
            'ret_url' => $this->ret_url,
            'trade_no' => $this->trade_no,
            'mer_trace' => $this->mer_trace,
            'pay_type' => $this->pay_type,
            'gate_id' => $this->gate_id,
            'amount' => $this->amount,
            'expire_time' => $this->expire_time,
        ];
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}