<?php

namespace AgilePayments\bin\extend\umf\api;

/**
 * 子商户绑卡信息查询接口
 */
class MerAccessMemberBindCardInfo
{
    public $data;

    protected $mer_id;
    protected $version = '1.0';
    protected $method = 'GET';

    public $user_id;


    public function getUrl()
    {
        return 'merAccess/member/bindCardInfo';
    }

    public function getData()
    {
        return [
            'mer_id'	=> $this->mer_id,
            'version'	=> $this->version,
            'user_id'	=> $this->user_id,
        ];
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}