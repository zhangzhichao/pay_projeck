<?php

namespace AgilePayments\bin\extend\umf\api;

class MerAccessRegisterSubmer
{
    public $data;

    protected $mer_id; // 商户编号
    protected $version = '1.0';
    public $notify_url;
    public $sub_mer_id;
    public $order_id;
    public $sub_mer_name;
    public $sub_mer_type;
    public $sub_mer_alias;
    public $cert_type;
    public $cert_id;
    public $legal_name;
    public $identity_id;
    public $contacts_name;
    public $mobile_id;
    public $email = '';
    public $address = '';


    public function getUrl()
    {
        return 'merAccess/register/submer';
    }

    public function getData()
    {
        return [
            'mer_id'        => $this->mer_id,
            'notify_url'    => $this->notify_url,
            'sub_mer_id'    => $this->sub_mer_id,
            'order_id'      => $this->order_id,
            'sub_mer_name'  => $this->sub_mer_name,
            'version'       => $this->version,
            'mer_date'      => date('Ymd'),
            'sub_mer_type'  => $this->sub_mer_type,
            'sub_mer_alias' => $this->sub_mer_alias,
            'cert_type'     => $this->cert_type,
            'cert_id'       => $this->cert_id,
            'legal_name'	=> $this->legal_name,
            'identity_id'	=> $this->identity_id,
            'contacts_name'	=> $this->contacts_name,
            'mobile_id'	    => $this->mobile_id,
            'email'	        => $this->email,
            'address'	    => $this->address,
        ];
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}