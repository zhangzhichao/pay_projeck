<?php

namespace AgilePayments\bin\extend\umf\api;

class MerAccessConsumeGenaralOrder
{
    public $data;

    protected $method = 'POST';
    protected $version = '1.0';
    protected $order_type = '2';
    protected $order_channel = '2';
    protected $mer_id; // 商户编号
    public $order_id;
    public $amount;
    public $split_cmd;

    public function getUrl()
    {
        return 'merAccess/consume/genaralOrder';
    }

    public function getData()
    {
        return [
            'mer_id'        => $this->mer_id,
            'version'       => '1.0',
            'order_id'      => $this->order_id,
            'mer_date'      => date('Ymd'),
            'order_type'    => $this->order_type,
            'amount'        => $this->amount,// $pay_price,
            'order_channel' => $this->order_channel,
            'split_cmd'     => $this->split_cmd,
        ];
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}