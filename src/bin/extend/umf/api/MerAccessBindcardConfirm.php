<?php

namespace AgilePayments\bin\extend\umf\api;

/**
 * 确认绑卡
 */
class MerAccessBindcardConfirm
{
    public $data;

    protected $mer_id;
    protected $version = '1.1';
    protected $method = 'POST';

    public $trade_no;
    public $user_id;
    public $verify_code;


    public function getUrl()
    {
        return 'merAccess/bindcard/confirm';
    }

    public function getData()
    {
        return [
            'mer_id'	    => $this->mer_id,
            'version'	    => '1.1',
            'trade_no'	    => $this->trade_no,
            'user_id'	    => $this->user_id,
            'verify_code'	=> $this->verify_code,
        ];
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}