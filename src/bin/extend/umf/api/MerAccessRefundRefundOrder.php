<?php

namespace AgilePayments\bin\extend\umf\api;

/**
 * 退款
 */
class MerAccessRefundRefundOrder
{
    public $data;

    protected $mer_id;
    protected $version = '1.0';
    protected $method = 'POST';
    protected $fee_payer = '1';

    public $notify_url;
    public $order_id;
    public $amount;
    public $ori_order_id;
    public $ori_mer_date;
    public $refund_cmd;


    public function getUrl()
    {
        return 'merAccess/refund/refundOrder';
    }

    public function getData()
    {
        return [
            'mer_id'	        => $this->mer_id,
            'version'	        => $this->version,
            'notify_url'        => $this->notify_url,
            'order_id'          => $this->order_id,
            'mer_date'          => date("Ymd"),
            'amount'            => $this->amount,
            'ori_order_id'      => $this->ori_order_id,
            'ori_mer_date'      => date('Ymd',$this->ori_mer_date),
            'ori_mer_trace'     => $this->$ori_order_id . '_pay',
            'fee_payer'         => $this->fee_payer,
            'refund_cmd'        => $this->refund_cmd,
        ];
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}