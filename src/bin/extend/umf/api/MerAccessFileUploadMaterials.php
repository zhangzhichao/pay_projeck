<?php

namespace AgilePayments\bin\extend\umf\api;

/**
 * 提交企业子商户资质文件
 */
class MerAccessFileUploadMaterials
{
    public $data;

    protected $mer_id;
    protected $version = '1.0';
    protected $method = 'POST';

    public $notify_url;
    public $user_id;
    public $user_type;
    public $md5_cipher;


    public function getUrl()
    {
        return 'merAccess/file/upload/materials';
    }

    public function getData()
    {
        return [
            'mer_id'        => $this->mer_id,
            'notify_url'    => $this->notify_url,
            'version'       => $this->version,
            'user_id'       => $this->user_id,
            'user_type'     => $this->user_type, // 2：个体商户，3：企业商户
            'md5_cipher'    => md5($this->md5_cipher),
        ];
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}