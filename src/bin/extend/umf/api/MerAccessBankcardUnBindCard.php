<?php

namespace AgilePayments\bin\extend\umf\api;

/**
 * 提现协议解绑
 */
class MerAccessBankcardUnBindCard
{
    public $data;

    protected $mer_id;
    protected $version = '1.0';
    protected $method = 'POST';

    public $user_id;
    public $order_id;
    public $user_type;
    public $p_agreement_id;
    public $last_four_cardid;


    public function getUrl()
    {
        return 'merAccess/bankcard/unBindCard';
    }

    public function getData()
    {
        return [
            'mer_id'	=> $this->mer_id,
            'user_id'	=> $this->user_id,
            'order_id'	=> $this->order_id,
            'user_type'	=> $this->user_type,// 用户类型：1-个人；2-个体；3-企业
            'mer_date'	=> date('Ymd'),
            'p_agreement_id'	=> $this->p_agreement_id,
            'last_four_cardid'	=> $this->last_four_cardid,
        ];
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}