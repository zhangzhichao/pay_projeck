<?php

namespace AgilePayments\bin\extend\umf\api;

/**
 * 银行卡支付信息（接口一）
 */
class MerAccessPaymentQuickOrder
{
    public $data;

    protected $mer_id;
    protected $version = '1.0';
    protected $method = 'POST';

    public $notify_url;
    public $trade_no;
    public $mer_cust_id;
    public $mer_trace;
    public $amount;
    public $card_id;
    public $p_agreement_id = '';


    public function getUrl()
    {
        return 'merAccess/payment/quick/order';
    }

    public function getData()
    {
        $data = [
            'mer_id' => $this->mer_id,
            'version' => $this->version,
            'notify_url' => $this->notify_url,
            'trade_no' => $this->trade_no,
            'mer_cust_id' => $this->mer_cust_id,
            'mer_trace' => $this->mer_trace,
            'amount' => $this->amount,
        ];
        if ($this->p_agreement_id != '') {
            $data['p_agreement_id'] = $this->p_agreement_id;
        }else{
            $date['card_id'] = $this->card_id;
        }

        return $date;
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}