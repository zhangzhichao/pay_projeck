<?php

namespace AgilePayments\bin\extend\umf\api;

class MerAccessMemberMemberInfo
{
    public $data;

    protected $mer_id;
    protected $version = '1.0';
    protected $method = 'GET';
    
    public $sub_mer_id;


    public function getUrl()
    {
        return 'merAccess/member/memberInfo';
    }

    public function getData()
    {
        return [
            'mer_id'        => $this->mer_id,
            'version'       => '1.0',
            'sub_mer_id'    => $this->sub_mer_id,
        ];
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}