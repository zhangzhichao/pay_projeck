<?php

namespace AgilePayments\bin\extend\umf\api;

/**
 * 绑定企业对公结算账户
 */
class MerAccessBindcardEnterprise
{
    public $data;

    protected $mer_id;
    protected $version = '1.0';
    protected $method = 'POST';

    public $order_id;
    public $user_id;
    public $bank_account_name;
    public $bank_account;
    public $gate_id;
    public $notify_url;
    public $mobile_id;
    public $bank_branch_name;
    public $bank_branch_code;


    public function getUrl()
    {
        return 'merAccess/bindcard/enterprise';
    }

    public function getData()
    {
        return [
            'mer_id'	        => $this->mer_id,
            'version'	        => $this->version,
            'order_id'	        => $this->order_id,
            'mer_date'	        => date('Ymd'),
            'user_id'	        => $this->user_id,
            'bank_account_name'	=> $this->bank_account_name,
            'bank_account'	    => $this->bank_account,
            'gate_id'	        => $this->gate_id,
            'notify_url'	    => $this->notify_url,
            'mobile_id'	        => $this->mobile_id,
            'bank_branch_name'	=> $this->bank_branch_name,
            'bank_branch_code'	=> $this->bank_branch_code,
        ];
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}