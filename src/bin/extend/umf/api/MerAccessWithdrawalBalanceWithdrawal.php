<?php

namespace AgilePayments\bin\extend\umf\api;

/**
 * 账户余额提现
 */
class MerAccessWithdrawalBalanceWithdrawal
{
    public $data;

    protected $mer_id;
    protected $version = '1.0';
    protected $method = 'POST';
    public $order_id;
    public $user_id;
    public $amount;
    public $remark;
    public $p_agreement_id;
    public $notify_url;
    public $mobile_id;


    public function getUrl()
    {
        return 'merAccess/withdrawal/balanceWithdrawal';
    }

    public function getData()
    {
        return [
            'mer_id'            => $this->mer_id,
            'version'           => $this->version,
            'order_id'          => $this->order_id,
            'mer_date'          => date('Ymd'),
            'user_id'           => $this->user_id,
            'amount'            => $this->amount,
            'remark'            => $this->remark,
            'p_agreement_id'    => $this->p_agreement_id,
            'notify_url'        => $this->notify_url,
            'mobile_id'         => $this->mobile_id,
        ];
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}