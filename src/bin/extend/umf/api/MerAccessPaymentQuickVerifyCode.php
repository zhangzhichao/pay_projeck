<?php

namespace AgilePayments\bin\extend\umf\api;

/**
 * 获取银行卡支付验证码
 */
class MerAccessPaymentQuickVerifyCode
{
    public $data;

    protected $mer_id;
    protected $version = '1.0';
    protected $method = 'POST';

    public $notify_url;
    public $trade_no;
    public $mer_trace;
    public $bank_mobile_id;
    public $card_id;
    public $card_holder;
    public $identity_type;
    public $identity_code;
    public $valid_date;
    public $cvv2;
    public $p_agreement_id = '';


    public function getUrl()
    {
        return 'merAccess/payment/quick/verifyCode';
    }

    public function getData()
    {
        $data = [
            'mer_id'        => $this->mer_id,
            'version'       => $this->version,
            'notify_url'    => $this->notify_url,
            'trade_no'      => $this->trade_no,
            'mer_trace'     => $this->mer_trace,
            'bank_mobile_id' => $this->bank_mobile_id,
        ];

        if ($this->p_agreement_id == ''){
            $data['card_id'] = $this->card_id;
            $data['card_holder'] = $this->card_holder;
            $data['identity_type'] = $this->identity_type;
            $data['identity_code'] = $this->identity_code;
            $data['valid_date'] = $this->valid_date;
            $data['cvv2'] = $this->cvv2;
        }else{
            $data['p_agreement_id'] = $this->p_agreement_id;
        }

    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}