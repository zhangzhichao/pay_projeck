<?php

namespace AgilePayments\bin\extend\umf\api;

/**
 * 退款查询
 */
class MerAccessRefundOrderInfo
{
    public $data;

    protected $mer_id;
    protected $version = '1.0';
    protected $method = 'GET';

    public $mer_date;
    public $order_id;


    public function getUrl()
    {
        return 'merAccess/refund/orderInfo';
    }

    public function getData()
    {
        return [
            'mer_id'	=> $this->mer_id,
            'version'	=> $this->version,
            'mer_date'	=> $this->mer_date,
            'order_id'	=> $this->order_id,
        ];
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}