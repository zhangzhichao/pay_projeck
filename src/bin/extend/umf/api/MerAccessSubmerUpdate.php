<?php

namespace AgilePayments\bin\extend\umf\api;

/**
 * 子商户信息修改
 */
class MerAccessSubmerUpdate
{
    public $data;

    protected $mer_id;
    protected $method = 'POST';
    
    public $user_id;
    public $sub_mer_type;
    public $sub_mer_name;
    public $sub_mer_alias;
    public $legal_name;
    public $identity_id;
    public $contacts_name;
    public $mobile_id;


    public function getUrl()
    {
        return 'merAccess/submer/update';
    }

    public function getData()
    {
        return [
            'mer_id'    => $this->mer_id,
            'user_id'   => $this->user_id,
            'sub_mer_type'	=> $this->sub_mer_type,
            'sub_mer_name'	=> $this->sub_mer_name,
            'sub_mer_alias'	=> $this->sub_mer_alias,
            'legal_name'	=> $this->legal_name,
            'identity_id'	=> $this->identity_id,
            'contacts_name'	=> $this->contacts_name,
            'mobile_id'	=> $this->mobile_id,
        ];
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}