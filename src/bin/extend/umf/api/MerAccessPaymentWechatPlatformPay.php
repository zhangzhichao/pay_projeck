<?php

namespace AgilePayments\bin\extend\umf\api;

class MerAccessPaymentWechatPlatformPay
{
    public $data;
    protected $method = 'POST';

    protected $version = '1.0';
    protected $pay_type = 'ALIPAY';
    protected $sub_pay_type = 'ALIPAY';
    protected $multiple_subject = '1';
    protected $mer_id;
    protected $user_ip;
    public $notify_url;
    public $trade_no;
    public $mer_trace;
    public $consumer_id;
    public $openid;
    public $appid;
    public $amount;
    public $goods_inf;
    public $remark;


    public function getUrl()
    {
        return 'merAccess/payment/wechatPlatformPay';
    }

    public function getData()
    {
        return [
            'mer_id'	    => $this->mer_id,
            'notify_url'	=> $this->notify_url,
            'version'	    => $this->version,
            'trade_no'	    => $this->trade_no,
            'mer_trace'	    => $this->mer_trace,
            'pay_type'	    => $this->pay_type,
            'sub_pay_type'  => $this->sub_pay_type,
            'user_ip'       => $this->getIP()??'',
            'consumer_id'   => $this->consumer_id,
            'multiple_subject'  => $this->multiple_subject,
            'openid'        => $this->openid,
            'appid'         => $this->appid,
            'amount'	    => $this->amount,
            'goods_inf'	    => $this->goods_inf,
            'remark'	    => $this->remark,
        ];
    }

    private function getIP()
    {
        static $realip;
        if (isset($_SERVER)){
            if (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
                $realip = $_SERVER["HTTP_X_FORWARDED_FOR"];
            } else if (isset($_SERVER["HTTP_CLIENT_IP"])) {
                $realip = $_SERVER["HTTP_CLIENT_IP"];
            } else {
                $realip = $_SERVER["REMOTE_ADDR"];
            }
        } else {
            if (getenv("HTTP_X_FORWARDED_FOR")){
                $realip = getenv("HTTP_X_FORWARDED_FOR");
            } else if (getenv("HTTP_CLIENT_IP")) {
                $realip = getenv("HTTP_CLIENT_IP");
            } else {
                $realip = getenv("REMOTE_ADDR");
            }
        }
        return $realip;
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}