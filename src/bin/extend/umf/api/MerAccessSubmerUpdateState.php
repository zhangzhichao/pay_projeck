<?php

namespace AgilePayments\bin\extend\umf\api;

/**
 * 子商户状态管理
 */
class MerAccessSubmerUpdateState
{
    public $data;

    protected $mer_id;
    protected $version = '1.0';
    protected $method = 'POST';
    
    public $sub_mer_id;
    public $sub_mer_state;


    public function getUrl()
    {
        return 'merAccess/submer/updateState';
    }

    public function getData()
    {
        return [
            'mer_id'        => $this->mer_id,
            'version'       => $this->version,
            'sub_mer_id'    => $this->sub_mer_id,
            'sub_mer_state'	=> $this->sub_mer_state,
        ];
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}