<?php

namespace AgilePayments\bin\extend\umf\api;

/**
 * 账户余额提现
 */
class MerAccessRegisterPersonal
{
    public $data;

    protected $mer_id;
    protected $version = '1.0';
    protected $method = 'POST';
    protected $user_type = '1';
    protected $identity_type = '1';
    public $notify_url;
    public $order_id;
    public $mer_cust_id;
    public $cust_name;
    public $mobile_id;
    public $identity_code;
    public $sub_mer_alias;



    public function getUrl()
    {
        return 'merAccess/register/personal';
    }

    public function getData()
    {
        return [
            'mer_id'        => $this->mer_id,
            'notify_url'    => $this->notify_url,
            'version'       => $this->version,
            'order_id'      => $this->order_id,
            'mer_date'      => date('Ymd'),
            'mer_cust_id'   => $this->mer_cust_id,
            'user_type'     => $this->user_type,
            'mobile_id'     => $this->mobile_id,
            'cust_name'     => $this->cust_name,
            'identity_type' => $this->identity_type,
            'identity_code' => $this->identity_code,
            'sub_mer_alias' => $this->sub_mer_alias,
        ];
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}