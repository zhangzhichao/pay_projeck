<?php

namespace AgilePayments\bin\extend\umf\api;

/**
 * 查询提现交易
 */
class MerAccessWithdrawalOrderInfo
{
    public $data;

    protected $mer_id;
    protected $version = '1.0';
    protected $method = 'GET';
    public $order_id;
    public $mer_date;


    public function getUrl()
    {
        return 'merAccess/withdrawal/orderInfo';
    }

    public function getData()
    {
        return [
            'mer_id'            => $this->mer_id,
            'version'           => $this->version,
            'order_id'          => $this->order_id,
            'mer_date'          => $this->mer_date,
        ];
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}