<?php

namespace AgilePayments\bin\extend\umf\api;

/**
 * 关闭支付订单
 */
class MerAccessConsumeCloseOrder
{
    public $data;

    protected $mer_id;
    protected $version = '1.0';
    protected $method = 'POST';

    public $order_id;
    public $mer_date;

    public function getUrl()
    {
        return 'merAccess/consume/closeOrder';
    }

    public function getData()
    {
        return [
            'mer_id'	=> $this->mer_id,
            'version'	=> $this->version,
            'order_id'	=> $this->order_id,
            'mer_date'	=> $this->mer_date,
        ];
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}