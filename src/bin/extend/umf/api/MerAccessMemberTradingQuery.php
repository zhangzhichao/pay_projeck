<?php

namespace AgilePayments\bin\extend\umf\api;

/**
 * 账户流水查询
 * @param $user_id int 联动侧user_id
 * @param $type int 查询类型 【1：子商户在途流水，2:子商户余额流水】
 * @param $page
 * @param $limit
 * @param $startDate string 查询时间范围（一年内，30天数据）
 * @param $endDate string 查询时间范围（一年内，30天数据）
 */
class MerAccessMemberTradingQuery
{
    public $data;

    protected $mer_id;
    protected $version = '1.1';
    protected $method = 'GET';

    public $user_id;
    public $acc_type;
    public $start_date;
    public $end_date;
    public $page_num;
    public $page_size;


    public function getUrl()
    {
        return 'merAccess/member/tradingQuery';
    }

    public function getData()
    {
        return [
            'mer_id'        => $this->mer_id,
            'version'       => $this->version,
            'amt_flag'      => '1',
            'user_id'       => $this->user_id,
            'acc_type'      => $this->acc_type,
            'start_date'    => $this->startDate,
            'end_date'      => $this->endDate,
            'page_num'      => $this->page,
            'page_size'     => $this->limit,
        ];
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}