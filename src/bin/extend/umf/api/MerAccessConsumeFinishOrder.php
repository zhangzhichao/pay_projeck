<?php

namespace AgilePayments\bin\extend\umf\api;

/**
 * 订单解冻
 */
class MerAccessConsumeFinishOrder
{
    public $data;

    protected $mer_id;
    protected $version = '1.0';
    protected $method = 'POST';

    public $trade_no;
    public $notify_url = '';


    public function getUrl()
    {
        return 'merAccess/consume/finishOrder';
    }

    public function getData()
    {
        return [
            'mer_id'	    => $this->mer_id,
            'version'	    => '1.0',
            'trade_no'	    => $this->trade_no,
            'notify_url'	=> $this->notify_url,
        ];
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}