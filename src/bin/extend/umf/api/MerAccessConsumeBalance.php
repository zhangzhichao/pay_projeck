<?php

namespace AgilePayments\bin\extend\umf\api;

/**
 * @deac 余额支付
 * @param string 支付账号 【1：结算用户，2：分佣账号】
 */
class MerAccessConsumeBalance
{
    public $data;

    protected $mer_id;
    protected $version = '1.0';
    protected $method = 'POST';

    public $notify_url;
    public $trade_no;
    public $mer_trace;
    public $user_id;
    public $amount;
    public $remark;


    public function getUrl()
    {
        return 'merAccess/consume/balance';
    }

    public function getData()
    {
        return [
            'mer_id'        => $this->mer_id,
            'version'       => $this->version,
            'notify_url'    => $this->notify_url,
            'trade_no'      => $this->trade_no,
            'mer_trace'     => $this->mer_trace,
            'user_id'       => $this->user_id,
            'amount'        => $this->amount,
            'remark'        => $this->remark,
        ];
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}