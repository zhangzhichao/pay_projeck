<?php

namespace AgilePayments\bin\extend\umf\api;

class MerAccessMemberBalance
{
    public $data;

    protected $mer_id;
    protected $version = '1.0';
    protected $method = 'GET';
    public $user_id;


    public function getUrl()
    {
        return 'merAccess/member/balance';
    }

    public function getData()
    {
        return [
            'mer_id' => $this->mer_id,
            'user_id' => $this->user_id,
            'version' => $this->version,
        ];
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}