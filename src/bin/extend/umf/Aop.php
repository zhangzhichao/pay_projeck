<?php

namespace AgilePayments\bin\extend\umf;

use AgilePayments\bin\AgilePaymentException;
use AgilePayments\bin\AgilePaymentLog;
use AgilePayments\Tool;
use GuzzleHttp\Client as GClient;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class Aop
{
    protected GClient $client;

    protected Request $request;

    protected Response $response;

    protected array $GConfig;

    public $base_url = 'https://b2b.umfintech.com/';

    private $config;

    public function __construct(\AgilePayments\Config $config)
    {
        $this->config = $config;
    }

    public function execute($request, array $GConfig = [])
    {
        $this->setGConfig($GConfig);
        $request->mer_id = $this->config->merId;
        $data = $request->getData();
        ksort($data);
        $this->GConfig['json'] = $data;

        $this->GConfig['headers']['Signature'] = $this->sign($data, $request->method);
        try {
            $this->client = new \GuzzleHttp\Client();
            if (strtoupper($request->method) == "GET"){
                $url = $request->getUrl() . '?' . http_build_query($data);
            }else{
                $url = $request->getUrl();
            }
            $this->request = new Request(strtoupper($request->method),  $this->base_url . $request->getUrl() . '?' . http_build_query($data));

            $this->response = $this->client->send($this->request, $this->GConfig);
        } catch (RequestException $exception) {
            throw new AgilePaymentException($exception, $exception->getCode(), func_get_args());
        }
        
        return json_decode($this->response->getBody()->__toString());

    }

    public function executeFile($request, array $GConfig = [])
    {
        $this->setGConfig($GConfig);
        $request->mer_id = $this->config->merId;
        $data = $request->getData();
        ksort($data);
        $this->GConfig['multipart'][] = ['name'=>'data','contents'=>json_encode($data)];

        $this->GConfig['headers']['Signature'] = $this->sign($data, $request->method);
        unset($this->GConfig['headers']['content-type']);
        try {
            $this->client = new \GuzzleHttp\Client();
            if (strtoupper($request->method) == "GET"){
                $url = $request->getUrl() . '?' . http_build_query($data);
            }else{
                $url = $request->getUrl();
            }
            $this->request = new Request(strtoupper($request->method),  $this->base_url . $request->getUrl() . '?' . http_build_query($data));
            $this->response = $this->client->send($this->request, $this->GConfig);
        } catch (RequestException $exception) {
            echo $exception->getMessage();
        }

        return json_decode($this->response->getBody()->__toString());

    }



    private function setGConfig($GConfig)
    {
        $this->GConfig['headers']['content-type'] = empty($GConfig['headers']['content-type']) ? 'application/json;charset=utf-8' : $GConfig['headers']['content-type'];
        $this->GConfig['timeout'] = empty($GConfig['timeout']) ? 5 : $GConfig['timeout'];
        $this->GConfig['verify'] = empty($GConfig['verify']) ? false : $GConfig['verify'];

        foreach ($GConfig as $key => $value) {
            if (is_array($value)){
                $this->GConfig[$key] = array_merge_recursive($this->GConfig[$key]??[], $value);
            }else{
                $this->GConfig[$key] = $value;
            }
        }

        $stack = new HandlerStack();
        $stack->setHandler(new CurlHandler());

        $stack->push(Middleware::mapRequest(function (RequestInterface $request) {
            parse_str($request->getUri()->getQuery(), $param);
            AgilePaymentLog::writeLog(['header'=>$request->getHeaders(),'centents' => $request->getBody()->getContents()], $this->config->platform . ' request : ' . $request->getUri()->getPath());
            $request->getBody()->rewind();
            return $request;
        }));

        $stack->push(Middleware::mapResponse(function (ResponseInterface $response) {
            AgilePaymentLog::writeLog(['header'=>$response->getHeaders(),'centents' => $response->getBody()->getContents()], $this->config->platform . ' response');
            $response->getBody()->rewind();
            return $response;
        }));
//        $this->GConfig['handler'] = $stack;
    }

    /**
     * 参数签名
     * @param array $data
     * @param string $method
     * @return string
     */
    private function sign(array $data, string $method = 'get'):string
    {
        ksort($data);

        if (strtolower($method) == 'get') {
            $arg = http_build_query($data);
        } else {
            $arg = json_encode($data);
            $this->GConfig['headers']['Content-Length'] = strlen($arg);
        }
        $fp = fopen(Tool::getRootPath() . $this->config->serverPrivateCertPath, "rb");
        $priv_key = fread($fp, 8192);
        @fclose($fp);
        $pkeyid = openssl_get_privatekey($priv_key);

        @openssl_sign($arg, $signature, $pkeyid);
        @openssl_free_key($pkeyid);
        return base64_encode($signature) ?? '';
    }

    public function encrypt($data)
    {
        $cert_file = Tool::getRootPath() . $this->config->payRootCertPath;
        if(!file_exists($cert_file)){
            die("传入的公钥文件不存在,公钥文件绝对路径 ".$cert_file);
        }
        $fp = fopen($cert_file,"r");
        $public_key = fread($fp,8192);
        $public_key = openssl_get_publickey($public_key);
        openssl_public_encrypt($data,$crypttext,$public_key);
        $encryptDate = base64_encode($crypttext);
        return $encryptDate;

    }
    /**
     * 联动公钥验证签名
     * @param $plain
     * @param $sign
     * @return bool
     */
    public function verifySign($plain, $sign) {
        $pubKey = file_get_contents(Tool::getRootPath() . $this->config->payRootCertPath);
        $pkeyid = openssl_get_publickey($pubKey);
        $result = (bool)openssl_verify($plain, base64_decode($sign), $pkeyid);
        openssl_free_key($pkeyid);
        return $result ? true : false;
    }

    public function returnSuccess()
    {
        echo 'success';
    }

    public function getIP()
    {
        static $realip;
        if (isset($_SERVER)){
            if (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
                $realip = $_SERVER["HTTP_X_FORWARDED_FOR"];
            } else if (isset($_SERVER["HTTP_CLIENT_IP"])) {
                $realip = $_SERVER["HTTP_CLIENT_IP"];
            } else {
                $realip = $_SERVER["REMOTE_ADDR"];
            }
        } else {
            if (getenv("HTTP_X_FORWARDED_FOR")){
                $realip = getenv("HTTP_X_FORWARDED_FOR");
            } else if (getenv("HTTP_CLIENT_IP")) {
                $realip = getenv("HTTP_CLIENT_IP");
            } else {
                $realip = getenv("REMOTE_ADDR");
            }
        }
        return $realip;
    }


}