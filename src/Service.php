<?php
namespace AgilePayments;

use AgilePayments\Config;
use think\Db;

class Service
{
    public $pay_type;
    public $pay_type_list;
    private $db;
    public function __construct()
    {
        if (class_exists('\think\facade\Db')){
            $this->db = new \think\facade\Db();
        }else{
            $this->db = new \think\Db();
        }

//        var_dump((array)$config->pay_type_config);

//        echo $config->frame_name;

//        $config->db::name()->join();
        
//        $this->pay_type = $pay_type;
//        if ($pay_type == ''){
//            throw new \Exception("初始化必须传入支付方式");
//        }
//        $this->pay_type_list = $this->getPayTypeList($pay_type);

    }

    /**
     * @desc 获取支付系统支付方式列表
     */
    public function getPayTypeList($pay_type = '')
    {
        $obj = $this->db::name('agile_pay_type') //
            ->alias('t')
            ->join('fa_agile_pay_type_config c','c.id = t.config_id','LEFT') //
            ->where('t.is_show', '1')
            ->where('t.is_use', '1')
            ->where('phone_type','1');
        if ($pay_type != ''){
            $where['t.name']  = ['=',$pay_type];
            $field = 't.id, t.config_id, t.name, t.title, t.image, c.app_id, c.app_key, c.app_secret, c.mid, c.tid, c.server_prv_cert, c.server_pub_cert, c.pay_pri_cert, c.pay_pub_cert';
            $list = $obj->field($field)->where($where)->find();
        }else{
            $where = [];
            $field = 't.id, t.name as payTypeKey, t.title as payTypeStr, t.image as payImage, t.is_jump, t.jump_path, t.platform,c.app_id, t.is_default as payTypeDef';
            $list = $obj->field($field)->where($where)->orderRaw('t.is_default = "1" desc, t.sort desc')->select();
            foreach ($list as &$value){
                $value['second'] = explode('.',$value['payTypeKey'])[1]??'';
            }
        }
        if (!$list){
            Tool::log('获取支付类型列表为空！','error');
            Tool::dingSend("出错了! \r\n获取支付类型列表为空！",'9527000001');
            throw new \Exception('初始化支付方式错误！');
            return false;
        }
        return $list;
//        try {

//
//        } catch (\Exception $e) {
//            Tool::log('获取支付类型列表失败！ | '.$e->getMessage(),'error');
//            Tool::dingSend("出错了! \r\n获取支付类型列表失败！ \r\n".$e->getMessage(),'9527000002');
//            return false;
//        }

    }


}