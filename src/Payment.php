<?php

namespace AgilePayments;

use AgilePayments\bin\format\PayReturn;
use AgilePayments\bin\format\RefundQueryFormat;
use AgilePayments\bin\format\ReturnFormat;
use app\api\controller\UMFintech;
use Exception;
use Alipay\EasySDK\Kernel\Factory;
use Alipay\EasySDK\Kernel\Util\ResponseChecker;
use AgilePayments\Config;
use AgilePayments\bin\AgilePaymentException;

/**
 *
 * @method PayReturn    pay(string $orderOutId, int $pay_price, int $out_time, string $notify_url, string $passback_params = '', string $title = '') 支付
 * @method ReturnFormat    query(string $outOrderId, string $date = '')   查询方法[云闪付必填时间]
 * @method RefundQueryFormat    refund(array $payInfo, int $price, string $notifyUrl = '')  退款方法
 * @method RefundQueryFormat    refundQuery(string $refund_no, string $outOrderId, string $date = '') 退款查询方法
 * @method ReturnFormat     close(string $outOrderId, string $time)   关闭支付方法
 *
 * @method bool     verifySign()  验签方法
 * @method arrau    getPayInfo($trade_no, $orderOutId, $type, $amount, $openid, $notify) 联动获取支付信息
 */
class Payment
{
    protected $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function __call($name, $arguments)
    {
        $platform = strtolower($this->config->platform);
        $payment = strtolower($this->config->payment);
        $functionName = ucfirst($payment);
        $class = "\\" . __NAMESPACE__ . "\\bin\\{$platform}\\{$payment}\\$functionName";


//        $platform = strtolower($this->config->platform);
//        $payment = ucfirst($this->config->payment);
//        $class = "AgilePaymentV2\\bin\\{$platform}\\{$payment}";


        if (!class_exists($class)) {
            throw new AgilePaymentException("调用类[$class]不存在！");
        }
        if (method_exists($class, $name)) {
            return call_user_func_array([new $class($this->config), $name], $arguments);
        } else {
            throw new AgilePaymentException("调用方法[$class::$name()]不存在！");
        }
    }

    /**
     * 给平台工具类
     * @param string $relativePaths
     * @return mixed
     * @throws AgilePaymentException
     */
    public function Util(string $relativePaths = '')
    {
        $platform = strtolower($this->config->platform);
        $class = "\\" . __NAMESPACE__ . "\\bin\\{$platform}{$relativePaths}\\Util";

        if (!class_exists($class)) {
            throw new AgilePaymentException("调用类[$class]不存在！");
        }

        return new $class($this->config);
//        if (method_exists($class, $functionName)) {
//            return call_user_func_array([new $class($this->config), $functionName], $args);
//        } else {
//            throw new AgilePaymentException("调用方法[$class::$functionName()]不存在！");
//        }
    }
}
