<?php

namespace AgilePayments;

/**
 * 支付配置类
 */
class Config
{

    //支付配置编号
    public $id;
    //配置说明
    public $desc;
    //类型 [1,2] 1:密钥，2：证书
    public $type;
    //第三方应用编号
    public $appId;
    //第三方交易平台
    public $platform;
    //第三方支付平台
    public $payment;
    //第三方支付方式
    public $payType;
    //第三方应用键值
    public $appKey;
    //第三方应用密钥
    public $appSecret;
    //商家编号
    public $merId;
    //终端编号
    public $terId;
    //唤起程序appID
    public $subAppId;
    //订单号前缀
    public $orderNoPrefix;
    //服务端公钥地址
    public $serverPublicCertPath;
    //服务端私钥地址
    public $serverPrivateCertPath;
    //服务端公钥内容
    public $serverPublicCertValue;
    //服务端私钥内容
    public $serverPrivateCertValue;
    //第三方公钥地址
    public $payPublicCertPath;
    //第三方私钥地址
    public $payPrivateCertPath;
    //第三方公钥内容
    public $payPublicCertValue;
    //第三方私钥内容
    public $payPrivateCertValue;
    //第三方根证书地址
    public $payRootCertPath;
    //第三方根证书内容
    public $payRootCertValue;
    //是否为沙盒环境
    public $isSendBox;

    public function __construct($config = [])
    {
        global $agileuuid;
        $agileuuid = Tool::generateUUIDv4();
        if (!empty($config)) {
            $this->setParamByArray($config);
        }
    }

    private function setParamByArray($data): void
    {
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
    }

}